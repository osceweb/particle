Unordered findings how things can be done in particle, Needs to be cleaned over time

## Twig tweak + particle plain
```
  {% set view = drupal_view_result('key_resources_paragraph', 'gallery', node.id)|length %}
  {% if view > 0 %}
      {% embed '@containers/_box.twig'
          with {
          box_header: 'Key Resources',
          box_label: source,
          box_class_value: 'box box__borderless',
      }
      %}
          {% block box_main %}
              {{ drupal_view('key_resources_paragraph', 'gallery', node.id) }}
          {% endblock box_main %}
      {% endembed %}
  {% endif %}
```
## Twig tweak, load view as a block (still missing the section top border)
```
      {{ drupal_block('views_block:key_resources_paragraph-gallery', {
          views_label: 'Key Resources',
          context_mapping: {nid: '@node.node_route_context:node'}
      }) }}
```

## Twig tweak, with custom wrapper function
```
{{ view_block_in_section('key_resources_paragraph', 'gallery', { label: source }) }}
```

## Twig Helper function to render a view block with current nid as context <-- not used anymore
```
  public function osce_view($name, $display_id, array $configuration = []) {
    $block_id = 'views_block:' . $name . '-' . $display_id;

    $configuration += [
      'label_display' => BlockPluginInterface::BLOCK_LABEL_VISIBLE,
      // Set currently viewed node as context.
      'context_mapping' => ['nid' => '@node.node_route_context:node'],
    ];

    $twig_tweak = \Drupal::service('twig_tweak.twig_extension');
    return $twig_tweak->drupalBlock($name, $display_id, $configuration = []);
  }
``` 

## Twig helpers used to render views in boxes/sections <-- They were not flexible enough and too specific
```

  public function viewBlockInElement($element, $name, $display_id, array $configuration = [], $wrapper = TRUE) {
    $block_id = 'views_block:' . $name . '-' . $display_id;
    $defaults = [
      'label' => FALSE,
    ];

    $configuration += [
      'label_display' => BlockPluginInterface::BLOCK_LABEL_VISIBLE,
      // Set currently viewed node as context.
      'context_mapping' => ['nid' => '@node.node_route_context:node'],
    ];

    $configuration = array_merge($defaults, $configuration);

    /** @var \Drupal\Core\Block\BlockPluginInterface $block_plugin */
    $block_plugin = \Drupal::service('plugin.manager.block')
      ->createInstance($block_id, $configuration);

    // Inject runtime contexts.
    if ($block_plugin instanceof ContextAwarePluginInterface) {
      $contexts = \Drupal::service('context.repository')->getRuntimeContexts($block_plugin->getContextMapping());
      \Drupal::service('context.handler')->applyContextMapping($block_plugin, $contexts);
    }

    if (!$block_plugin->access(\Drupal::currentUser())) {
      return;
    }

    $content = $block_plugin->build();

    if ($content && !Element::isEmpty($content)) {
      $label = isset($configuration['label']) ? $configuration['label'] : FALSE;
      $title = isset($configuration['title']) ? $configuration['title'] : $content["#title"]["#markup"];

      if ($wrapper) {
        $build = [
          '#type' => 'pattern',
          '#id' => $element,
          '#attributes' => [],
          '#contextual_links' => [],
          '#configuration' => $block_plugin->getConfiguration(),
          '#plugin_id' => $block_plugin->getPluginId(),
          '#base_plugin_id' => $block_plugin->getBaseId(),
          '#derivative_plugin_id' => $block_plugin->getDerivativeId(),
          '#' . $element . '_main' => $content,
          '#' . $element . '_label' => $label,
          '#' . $element . '_header' => $title,
        ];
      }
      else {
        $build = $content;
      }
    }
    else {
      // Preserve cache metadata of empty blocks.
      $build = [
        '#markup' => '',
        '#cache' => isset($content['#cache']) ? $content['#cache'] : [],
      ];
    }

    if (!empty($content)) {
      CacheableMetadata::createFromRenderArray($build)
        ->merge(CacheableMetadata::createFromRenderArray($content))
        ->applyTo($build);
    }

    return $build;
  }

  /**
   * Renders a view block in a box.
   *
   * @param mixed $name
   *   The view name.
   * @param mixed $display_id
   *   The string of view id to render.
   * @param array $configuration
   *   (Optional) Pass on any configuration to the plugin block.
   * @param bool $wrapper
   *   (Optional) Whether or not use block template for rendering.
   *
   * @return null|array
   *   A render array for the block or NULL if the block cannot be rendered.
   */
  public function viewBlockInBox($name, $display_id, array $configuration = [], $wrapper = TRUE) {
    return $this->viewBlockInElement('box', $name, $display_id, $configuration, $wrapper);
  }

  public function viewBlockInSection($name, $display_id, array $configuration = [], $wrapper = TRUE) {
    return $this->viewBlockInElement('section', $name, $display_id, $configuration, $wrapper);
  }
```