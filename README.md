# OSCE Themeing with particle

Our target is to work more in templates and less in UI.
- To realise that we make heavy use of the [Twig Tweak](https://www.drupal.org/project/twig_tweak) module - see [Cheatsheet](https://www.drupal.org/docs/8/modules/twig-tweak/cheat-sheet-8x-2x).
- We create custom Twig Tweak extensions in `Drupal\particle\TwigExtension`
- We use UI Patterns for container blocks to use them in render arrays and views.

## Setup your osce installation for working with particle

1. `git clone git@bitbucket.org:osceweb/particle.git` particle to `docroot/modules/custom/particle`
2. Setup particle `cd docroot/modules/custom/particle && npm install && npm run setup && npm start`
3. To update the drupal specific files run `npm run build:drupal`

Setup particle [Original particle README.md with install instructions](README-PARTICLE.md)

@TODO: Fix component-library creation: After every `npm run build:drupal` the component library in `particle.info.yml` is overwritten. Comment protons & layouts as a temporary fix, or contribute a fix here: https://github.com/phase2/particle/issues/503

## Template based themeing
See as example `docroot/themes/osce_base/templates/node--homepage--full.html.twig`

## How To?

### … provide a particle component for render arrays?
We use UI Patterns to provide components for render arrays. To annotate a new element you need to:
- Create a [my-component].ui-pattern.yml file in `source/default/_patterns/[XX-folder]/[my-component]/`
- Add `import './[my-component].ui_patterns.yml';` to the index.js file
- Run `npm run build:drupal`, which basically copies the pattern definition to apps/drupal/patterns/
- Clear the drupal cache `drush cr`

As an example have a look at the box component in `03-containers/box` and the rendering in the `Drupal\particle\TwigExtension.php:wrapInBox()`.
