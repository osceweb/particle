---
el: '<%= name %>'
title: '<%= name %>'
---

`source/_patterns/<%= cleanPatternType %>/<%= name %>/_<%= name %>.twig`

##Usage:

```
{% include '@<%= cleanPatternType %>/<%= name %>/_<%= name %>.twig' %}
```

or


```
{% include '@<%= cleanPatternType %>/<%= name %>/_<%= name %>.twig' 
with {
varname: '',
}
%}
```
