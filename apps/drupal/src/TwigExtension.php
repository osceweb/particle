<?php

namespace Drupal\particle;

use Drupal\Core\Entity\EntityMalformedException;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Language\LanguageManager;
use Drupal\node\NodeInterface;
use Drupal\views\Views;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Language\LanguageInterface;

/**
 * Twig extension with some useful functions and filters.
 *
 * Dependency injection is not used for performance reason.
 */
class TwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('osce_box', [$this, 'wrapInBox']),
      new \Twig_SimpleFunction('osce_section', [$this, 'wrapInSection']),
      new \Twig_SimpleFunction('osce_metadata', [$this, 'wrapInMetadata']),
      new \Twig_SimpleFunction('osce_bar', [$this, 'wrapInBar']),
      new \Twig_SimpleFunction('jira', [$this, 'linkToJira']),
      new \Twig_SimpleFunction('simple_link', [$this, 'simpleLink']),
      new \Twig_SimpleFunction('osce_more', [$this, 'linkViewAll']),
      new \Twig_SimpleFunction('osce_translations', [$this, 'renderTranslationLinks']),
      new \Twig_SimpleFunction('osce_tabs', [$this, 'wrapInTabs']),
      new \Twig_SimpleFunction('osce_url', [$this, 'toUrl']),
      new \Twig_SimpleFunction('osce_language', [$this, 'listOfLanguages']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    $filters = [
      new \Twig_SimpleFilter('view_mode', [$this, 'viewMode']),
    ];
    return $filters;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'osce_toolkit';
  }

  /**
   * Returns a render array for entity, field list or field item.
   *
   * @param mixed $object
   *   The object to build a render array from.
   * @param string $view_mode
   *  the name of a view mode
   * @return array
   *   A render array to represent the object.
   */
  public function viewMode($object, $view_mode = 'default') {
    if (isset($object["#items"])) {
      for ($i = 0; $i < count($object["#items"]->getValue()); $i++) {
        $object[$i]['#view_mode'] = $view_mode;
      }
    }
    return $object;
  }

  public function listOfLanguages(NodeInterface $node) {
    $links = [];

    /** @var \Drupal\Core\Language\LanguageManagerInterface $language_manager */
    $language_manager = \Drupal::service('language_manager');
    // get the native language name
    $standard_list = $language_manager->getStandardLanguageList();

    /** @var LanguageInterface $language */
    foreach ($node->getTranslationLanguages() as $language) {
      /** @var TranslatableInterface $translation */
      $translation = $node->getTranslation($language->getId());

      try {
        $link = [
          'label' => isset($standard_list[$language->getId()]) ? $standard_list[$language->getId()][1] : $language->getName(),
          'link' => $translation->toUrl()
        ];
        if ($language_manager->getCurrentLanguage()->getId() == $language->getId()) {
          $link['active'] = 'yes';
        }
        $links[] = $link;
      } catch (EntityMalformedException $e) {
      }
    }
    // sort links by label
    if (!empty($links)) {
      usort($links,
        function ($a, $b) {
          if ($a['label'] == $b['label']) {
            return 0;
          }
          return ($a['label'] < $b['label']) ? -1 : 1;
        }
      );
    }

    return [
      '#type' => 'pattern',
      '#id' => 'language',
      '#fields' => [
        'items' => $links,
      ],
    ];
  }

  public function toUrl($uri, array $options = []) {
    return Url::fromUri($uri, $options);
  }

  public function wrapInTabs(array $tabs, array $options = []) {

    $items = [];

    foreach ($tabs as $tab) {
      $items[] = [
        'styled_tab_title' => $tab['title'],
        'styled_tab_content' => $tab['content'],
        'styled_tab_id' => isset($tab['tab_id']) ? $tab['tab_id'] : str_ireplace(' ', '_', strtolower($tab['title'])),
      ];
    }

    return [
      '#type' => 'pattern',
      '#id' => 'tabs',
      '#fields' => [
        'items' => $items,
        'styled_tabs_nested' => isset($options['nested']) ? $options['nested'] : false,
      ],
    ];

  }

  /**
   * Renders translations links of an entity.
   *
   * @param \Drupal\Core\Entity\TranslatableInterface $entity
   * @return array
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function renderTranslationLinks(TranslatableInterface $entity) {

    /** @var \Drupal\Core\Language\LanguageManagerInterface $language_manager */
    $language_manager = \Drupal::service('language_manager');
    $standard_list = $language_manager->getStandardLanguageList();

    $links = [];
    foreach ($entity->getTranslationLanguages() as $lang) {
      /** @var TranslatableInterface $translation */
      $translation = $entity->getTranslation($lang->getId());
      $trans_uri = $translation->toUrl();

      // Set english name as first fallback translation.
      $name = $lang->getName();

      // Use a standard translation if no specific translation is set in db.
      if (isset($standard_list[$lang->getId()])) {
        $name = $standard_list[$lang->getId()][1];
      }

      $links[$lang->getId()] = [
        'title' => $name,
        'uri' => $trans_uri->toString(),
        'label' => $translation->label(),
        'active' => $translation->isDefaultTranslation(),
      ];
    }
    return [
      '#type' => 'pattern',
      '#id' => 'translation_links',
      '#fields' => [
        'set_id' => $entity->id(),
        'links' => $links,
      ],
    ];
  }

  /**
   * Render a read moe link.
   *
   * @param $base_path
   * @param array $params
   * @param string $text
   * @param string $type
   * @return array
   */
  public function linkViewAll($base_path, $text = 'View all', $params = [], $type = 'small') {

    // get current language
    $curr_langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();

    // base path for URL
    $url = $curr_langcode != 'en' ? "/$curr_langcode/$base_path" : "/$base_path";

    // add query parameters if existing
    if (!empty($params)) {
      $url .= '?';
      $i = 0;
      foreach ($params as $key => $param) {
        if (!empty($param)) {
          if ($i !== 0) {
            $url .= '&';
          }

          $url .= 'f[' . $i++ . ']=' . $key . ':' . $param;
        }
      }
    }

    return [
      '#type' => 'pattern',
      '#id' => 'more',
      '#attributes' => [],
      '#contextual_links' => [],
      '#readmore_text' => $text,
      '#readmore_url' => $url,
      '#readmore_type' => $type,
    ];
  }

  /**
   * Creates a simple link from caption and path.
   *
   * @param string $text
   *   The link caption.
   * @param string $path
   *   A path as string (e.g. "/elections").
   *
   * @return \Drupal\Core\Link
   *   Returns a link object.
   */
  public function simpleLink($text, $path) {
    return Link::fromTextAndUrl($text, Url::fromUserInput($path));
  }

  /**
   * Creates a link to jira.
   *
   * @param string $issue_id
   *   A jira issue ID.
   *
   * @return \Drupal\Core\Link
   *   Returns a link object to jira.
   */
  public function linkToJira($issue_id) {
    $issue_id = strtoupper($issue_id);
    return Link::fromTextAndUrl('See ' . $issue_id, Url::fromUri('https://osceweb.atlassian.net/browse/' . $issue_id));
  }

  /**
   * Wraps content into a box element.
   *
   * If the content is a render array, the section tries to extract the title
   * automatically from the content.
   *
   * @param array|string $content
   *   A render array or string to render.
   * @param array $options
   *   Possible options:
   *    [
   *     'title' => 'Section title',
   *     'label' => 'Section label',
   *     // Possible types: 'blue', 'dark', 'darkdavriant', 'shaded',
   *     // 'shaded-light', 'todo'.
   *     'type' => 'blue',
   *     'collapsible' => TRUE,
   *     'collapsed' => TRUE,
   *    ].
   *
   * @return array
   *   A render array with $content wrapped into a section.
   */
  public function wrapInBox($content, array $options = []) {

    if ($this->isEmpty($content)) {
      return [];
    }

    $defaults = [
      'label' => FALSE,
      'title' => FALSE,
      'type' => '',
      'collapsible' => FALSE,
      'collapsed' => FALSE,
    ];

    $options = array_merge($defaults, $options);

    if ($options['title'] === FALSE) {
      $options['title'] = $this->extractTitleFromElement($content);
    }

    return [
      '#type' => 'pattern',
      '#id' => 'box',
      '#attributes' => [],
      '#contextual_links' => [],
      '#box_main' => $content,
      '#box_label' => $options['label'],
      '#box_header' => $options['title'],
      '#box_type' => $options['type'],
      '#box_collapsible' => $options['collapsible'],
      '#box_collapsed' => $options['collapsed'],
    ];
  }

  /**
   * Wraps content into a section element.
   *
   * If the content is a render array, the section tries to extract the title
   * automatically from the content.
   *
   * @param array|string $content
   *   A render array or string to render.
   * @param array $options
   *   Possible options:
   *    [
   *     'title' => 'Section title',
   *     'label' => 'Section label',
   *    ].
   *
   * @return array
   *   A render array with $content wrapped into a section.
   */
  public function wrapInSection($content, array $options = []) {

    if ($this->isEmpty($content)) {
      return [];
    }

    $defaults = [
      'label'   => FALSE,
      'title'   => FALSE,
      'header'  => FALSE,
      'border'  => TRUE,
    ];

    $options = array_merge($defaults, $options);

    if ($options['title'] === FALSE) {
      $options['title'] = $this->extractTitleFromElement($content);
    }

    return [
      '#type' => 'pattern',
      '#id' => 'section',
      '#attributes' => [],
      '#contextual_links' => [],
      '#section_main'   => $content,
      '#section_label'  => $options['label'],
      '#section_title'  => $options['title'],
      '#section_header' => $options['header'],
      '#section_border' => $options['border'],
    ];
  }

  /**
   * Wraps content into a metadata list.
   *
   * If the content is a render array, the section tries to extract the title
   * automatically from the content.
   *
   * @param array|string $fields
   *   A list of render array fields.
   * @param array $options
   *   Possible options:
   *    [
   *     'list' => 'Type of list',
   *     'extra_class' => 'Extra CSS Classes',
   *    ].
   *
   * @return array
   *   A render array with $content wrapped into a section.
   */
  public function wrapInMetadata($fields, array $options = []) {
    $defaults = [
      'list' => FALSE,
      'extra_class' => FALSE,
    ];

    $options = array_merge($defaults, $options);

    $items = [];
    foreach ($fields as $icon => $field) {
      // Check if field is empty.
      if (!empty($field)) {
        $items[] =
          [
            'meta_data_txt' => $field,
            'meta_data_icon' => $icon != 'none' ? $icon : '',
          ];
      }
    }

    return [
      '#type' => 'pattern',
      '#id' => 'metadata_list',
      '#items' => $items,
      '#meta_data_list' => $options['list'] ?: '',
      '#meta_data_extra_class' => $options['extra_class'] ?: '',
    ];
  }

  /**
   * Wraps content into a bar.
   *
   * If the content is a render array, the section tries to extract the title
   * automatically from the content.
   *
   * @param array|string $content
   *   A render array or string to render.
   * @param array $options
   *   Possible options:
   *    [
   *     'classes' => 'CSS classes of wrapper',
   *     'content_classes' => 'CSS Classes of content',
   *    ].
   *
   * @return array
   *   A render array with $content wrapped into a section.
   */
  public function wrapInBar($content, array $options = []) {
    $defaults = [
      'classes' => FALSE,
      'content_classes' => FALSE,
    ];

    $options = array_merge($defaults, $options);

    return [
      '#type' => 'pattern',
      '#id' => 'bar',
      '#bar_content' => $content,
      '#bar_classes_value' => $options['classes'] ?: '',
      '#bar_content_classes_value' => $options['content_classes'] ?: '',
    ];
  }

  /**
   * Helper method to extract a title from a block or a view render array.
   *
   * @param mixed|array $element
   *   The element to check for. Currently supported:
   *      - block titles.
   *
   * @return string|boolen
   *   Either a title as a string or FALSE.
   */
  private function extractTitleFromElement($element) {
    // Load the title of a view display.
    if (isset($element['#type']) && ($element['#type'] == 'view')) {
      $view = Views::getView($element["#name"]);
      if (!$view || !$view->access($element["#display_id"])) {
        return FALSE;
      }
      return $view->getTitle();
    }

    // Load a block title.
    if (isset($element["#title"]["#markup"])) {
      return $element["#title"]["#markup"];
    }

    return FALSE;
  }

  /**
   * Check if a content variable is empty.
   *
   * Actually there is a core function for that (Element::isEmpty), but it does
   * not include the #weight attribute. So we need to implement our own check.
   *
   * @param mixed $element
   *   The render array to check.
   *
   * @return bool
   *   True, if empty.
   */
  private function isEmpty($element) {
    if (!is_array($element)) {
      return FALSE;
    }

    // Check if render array is empty.
    if (array_keys($element) === ['#cache', '#weight']) {
      return TRUE;
    }
    return FALSE;
  }

}
