<?php
namespace Drupal\particle\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;
use Drupal\node\Entity\Node;

/**
 * "Follow the source" block.
 *
 * @Block(
 *   id = "follow_the_source",
 *   admin_label = @Translation("Social Media follow the source icons"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Current Node"))
 *   }
 * )
 */
class FollowTheSource extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $node = $this->getContextValue("node");

    // Fetch the source and the related homepage page of the source.
    $source = $node->get('field_source')->entity;
    if (empty($source->get('field_term_root_page'))) {
      return [];
    }
    $source_node_link = $source->get('field_term_root_page')[0];
    if ($source_node_link->isExternal()) {
      return [];
    }
    $params = $source_node_link->getUrl()->getRouteParameters();
    if (!isset($params['node'])) {
      return [];
    }
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');
    $source_node = $node_storage->load($params['node']);

    if (empty($source_node->field_social_links)) {
      return [];
    }

    return [
      '#theme' => 'follow_the_source',
      '#label' => $source_node->field_follow_us_caption->value,
      '#links' => $source_node->field_social_links->view('full'),
    ];
  }

  /**
   * @inheritdoc
   */
  public function getCacheTags() {
    if (($node = \Drupal::routeMatch()->getParameter('node')) && ($node instanceof Node)) {
      // If there is node add its cachetag.
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    }
    else {
      // Return default tags instead.
      return parent::getCacheTags();
    }
  }

  /**
   * @inheritdoc
   */
  public function getCacheContexts() {
    // If you depend on \Drupal::routeMatch(), you must set context of this
    // block with 'route' context tag to rebuild it for every new route.
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }
}
