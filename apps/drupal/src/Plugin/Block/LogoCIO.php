<?php
namespace Drupal\particle\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * OSCE Chairmanship logo as a block.
 *
 * @Block(
 *   id = "logo_cio",
 *   admin_label = @Translation("Logo of the current Chairmanship"),
 * )
 */
class LogoCIO extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Easy to test at the end of the year: date("Y") + 1.
    $current_year = date("Y");
    return [
      '#type' => 'link',
      '#title' => $this->t('Chairmanship'),
      '#url' => Url::fromURI('https://www.osce.org/chairmanship'),
      '#cache' => [
        'tags' => ['date:year'],
      ],
      '#options' => [
        'attributes' => [
          'class' => [
            'logo',
            'logo--cio',
            'logo--chairmanship--' . $current_year,
          ],
        ],
      ],
    ];
  }
}
