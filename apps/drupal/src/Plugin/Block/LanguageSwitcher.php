<?php

namespace Drupal\particle\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;

/**
 * The osce specific "Switch language" block.
 *
 * @Block(
 *   id = "languageswitcher_osce",
 *   admin_label = @Translation("Language switcher OSCE")
 * )
 */
class LanguageSwitcher extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $entity = FALSE;
    if (\Drupal::routeMatch()->getParameter('node')) {
      $entity = \Drupal::routeMatch()->getParameter('node');
    }
    elseif (\Drupal::routeMatch()->getParameter('media')) {
      $entity = \Drupal::routeMatch()->getParameter('media');
    }

    if ($entity && $entity->id()) {
      $languages = [];

      /** @var \Drupal\Core\Language\LanguageInterface[] $languages */
      $entity_languages = $entity->getTranslationLanguages();

      /** @var \Drupal\Core\Language\LanguageInterface $active_language */
      $active_language = $entity->language();

      $language_manager = \Drupal::service('language_manager');
      $standard_list = $language_manager->getStandardLanguageList();
      $path_matcher = \Drupal::service('path.matcher');

      $route_name = $path_matcher->isFrontPage() ? '<front>' : '<current>';
      $type = $this->getDerivativeId();

      // Get translated language names.
      $links = $language_manager->getLanguageSwitchLinks($type, Url::fromRoute($route_name));

      foreach ($entity_languages as $key => $lang) {
        if (!$entity->getTranslation($key)->isPublished() && \Drupal::currentUser()->isAnonymous()) {
          continue;
        }

        $is_active = FALSE;
        if ($lang->getId() == $active_language->getId()) {
          $is_active = TRUE;
        }

        // Set english name as first fallback translation.
        $name = $lang->getName();

        // Use a standard translation if no specific translation is set in db.
        if (isset($standard_list[$lang->getId()])) {
          $name = $standard_list[$lang->getId()][1];
        }

        // Use a translated string, if it differs from english title.
        if (isset($links->links[$lang->getId()]['title'])) {
          $translated_name = $links->links[$lang->getId()]['title'];
          if ($translated_name != $lang->getName()) {
            $name = $translated_name;
          }
        }

        $languages[$key] = [
          'id' => $lang->getId(),
          'name' => $name,
          'active' => $is_active,
          'url' => $entity->getTranslation($key)->url('canonical', ['absolute' => TRUE]),
        ];
      }
      return [
        '#type' => 'pattern',
        '#id' => 'language_switcher',
        '#url' => Url::createFromRequest(\Drupal::request())->setAbsolute(TRUE),
        '#fields' => [
          'languages' => $languages,
        ],

        // @todo Make cacheable in https://www.drupal.org/node/2232375.
        '#cache' => ['max-age' => 0],
      ];
    }
    return [];
  }

}
