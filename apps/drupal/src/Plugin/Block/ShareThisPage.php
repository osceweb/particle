<?php
namespace Drupal\particle\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;
use Drupal\node\Entity\Node;

/**
 * "Share this page" block.
 *
 * @Block(
 *   id = "share_this_page",
 *   admin_label = @Translation("Share this page buttons")
 * )
 */
class ShareThisPage extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $route_match = \Drupal::routeMatch();
    $title = \Drupal::service('title_resolver')->getTitle(\Drupal::request(), $route_match->getRouteObject());
    $title = urlencode($title);

    $share_url = Url::fromRoute('<current>', array(), array("absolute" => TRUE))->toString();
    $share_url = urlencode($share_url);

    return [
      '#type' => 'pattern',
      '#id' => 'social_media_share',
      '#social_media_url' => Url::createFromRequest(\Drupal::request())->setAbsolute(TRUE),
      '#networks' => [
        [
          'name' => 'Facebook',
          'icon' => 'facebook',
          'url' => 'https://www.facebook.com/sharer/sharer.php?u=' . $share_url,
        ],
        [
          'name' => 'Twitter',
          'icon' => 'twitter',
          'url' => 'https://twitter.com/intent/tweet?text=' . $title . '&url=' . $share_url,
        ],
        [
          'name' => 'LinkedIn',
          'icon' => 'linkedin',
          'url' => 'https://www.linkedin.com/shareArticle?mini=true&title=' . $title . '&url=' . $share_url,
        ],
      ],
    ];
  }

  /**
   * @inheritdoc
   */
  public function getCacheTags() {
    if (($node = \Drupal::routeMatch()->getParameter('node')) && ($node instanceof Node)) {
      // If there is node add its cachetag.
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    }
    else {
      // Return default tags instead.
      return parent::getCacheTags();
    }
  }

  /**
   * @inheritdoc
   */
  public function getCacheContexts() {
    // If you depend on \Drupal::routeMatch(), you must set context of this
    // block with 'route' context tag to rebuild it for every new route.
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }
}