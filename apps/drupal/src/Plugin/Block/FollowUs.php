<?php
namespace Drupal\particle\Plugin\Block;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Cache\Cache;
use Drupal\node\Entity\Node;

/**
 * "Follow us" block.
 *
 * @Block(
 *   id = "follow_us",
 *   admin_label = @Translation("Social Media follow us block")
 * )
 */
class FollowUs extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#type' => 'pattern',
      '#id' => 'social_media_follow_us',
      '#follow_us_networks' => [
        [
          'name' => 'Facebook',
          'icon' => 'facebook',
          'url' => 'https://www.facebook.com/osce.org',
        ],
        [
          'name' => 'Twitter',
          'icon' => 'twitter',
          'url' => 'https://www.twitter.com/osce',
        ],
        [
          'name' => 'Youtube',
          'icon' => 'youtube',
          'url' => 'https://www.youtube.com/osce',
        ],
        [
          'name' => 'LinkedIn',
          'icon' => 'linkedin',
          'url' => 'https://www.linkedin.com/company/osce',
        ],
        [
          'name' => 'Soundcloud',
          'icon' => 'soundcloud',
          'url' => 'https://www.soundcloud.com/osce',
        ],
        [
          'name' => 'Instagram',
          'icon' => 'instagram',
          'url' => 'https://www.instagram.com/osceorg',
        ],
      ],
    ];
  }

  /**
   * @inheritdoc
   */
  public function getCacheTags() {
    if (($node = \Drupal::routeMatch()->getParameter('node')) && ($node instanceof Node)) {
      // If there is node add its cachetag.
      return Cache::mergeTags(parent::getCacheTags(), array('node:' . $node->id()));
    }
    else {
      // Return default tags instead.
      return parent::getCacheTags();
    }
  }

  /**
   * @inheritdoc
   */
  public function getCacheContexts() {
    // If you depend on \Drupal::routeMatch(), you must set context of this
    // block with 'route' context tag to rebuild it for every new route.
    return Cache::mergeContexts(parent::getCacheContexts(), array('route'));
  }
}