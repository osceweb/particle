<?php

namespace Drupal\particle\Plugin\views\style;

use Drupal\views\Plugin\views\style\DefaultStyle;

/**
 * Accordion style plugin to render rows in accordion groups
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "osce_accordion",
 *   title = @Translation("OSCE Accordion"),
 *   help = @Translation("Displays rows in accordion groups."),
 *   theme = "osce_accordion",
 *   display_types = {"normal"}
 * )
 */
class Accordion extends DefaultStyle {
  protected $usesRowPlugin = TRUE;
}
