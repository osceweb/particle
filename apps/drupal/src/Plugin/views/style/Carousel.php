<?php

namespace Drupal\particle\Plugin\views\style;

use Drupal\views\Annotation\ViewsStyle;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * @ViewsStyle(
 *   id = "osce_carousel",
 *   title = @Translation("OSCE Carousel"),
 *   help = @Translation("Display a slider with multiple items."),
 *   theme = "osce_carousel",
 *   display_types = {"normal"}
 * )
 */
class Carousel extends StylePluginBase {
  protected $usesRowPlugin = TRUE;
}