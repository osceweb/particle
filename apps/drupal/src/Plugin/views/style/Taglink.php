<?php

namespace Drupal\particle\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\DefaultStyle;

/**
 * Accordion style plugin to render rows in accordion groups
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "osce_taglink",
 *   title = @Translation("OSCE Taglink"),
 *   help = @Translation("Displays links in a row."),
 *   theme = "osce_taglink",
 *   display_types = {"normal"}
 * )
 */
class Taglink extends DefaultStyle {
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['color'] = ['default' => 'white'];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['color'] = [
      '#type' => 'select',
      '#title' => t('Color scheme'),
      '#options' => [
        'black' => t('Black'),
        'blue' => t('Blue'),
        'green' => t('Green'),
        'red' => t('Red'),
        'white' => t('White'),
        'yellow' => t('Yellow'),
      ],
      '#default_value' => $this->options['color'],
      '#description' => t('The color scheme for the links.')
    ];
  }
}
