<?php

namespace Drupal\particle\Plugin\views\style;

use Drupal\views\Annotation\ViewsStyle;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * @ViewsStyle(
 *   id = "osce_gallery_listing",
 *   title = @Translation("OSCE Gallery Listing (2col)"),
 *   help = @Translation("Display a OSCE themed gallery listing."),
 *   theme = "osce_gallery_listing",
 *   display_types = {"normal"}
 * )
 */
class GalleryListing extends StylePluginBase {
  protected $usesRowPlugin = TRUE;
}