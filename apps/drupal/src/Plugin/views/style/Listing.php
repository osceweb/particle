<?php

namespace Drupal\particle\Plugin\views\style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Annotation\ViewsStyle;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * @ViewsStyle(
 *   id = "osce_listing",
 *   title = @Translation("OSCE Listing"),
 *   help = @Translation("Display a OSCE themed listing."),
 *   theme = "osce_listing",
 *   display_types = {"normal"}
 * )
 */
class Listing extends StylePluginBase {
  protected $usesRowPlugin = TRUE;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['format'] = ['default' => false];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['format'] = [
      '#type' => 'select',
      '#title' => t('Format'),
      '#options' => ['normal' => t('Normal'), 'highlighted' => t('Highlighted')],
      '#default_value' => $this->options['format'],
      '#description' => t('NOT WORKING - Hide the dots bellow the slider.')
    ];
  }

}