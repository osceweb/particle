<?php

namespace Drupal\particle\Plugin\views\style;

use Drupal\views\Annotation\ViewsStyle;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * @ViewsStyle(
 *   id = "osce_gallery",
 *   title = @Translation("OSCE Gallery"),
 *   help = @Translation("Display a OSCE themed gallery."),
 *   theme = "osce_gallery",
 *   display_types = {"normal"}
 * )
 */
class Gallery extends StylePluginBase {
  protected $usesRowPlugin = TRUE;
}