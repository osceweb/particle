<?php

namespace Drupal\particle\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;

/**
 * Plugin implementation of the 'file_download_link_translated' formatter.
 *
 * @FieldFormatter(
 *   id = "osce__download_link_translated",
 *   label = @Translation("OSCE Download link with file translations"),
 *   description = @Translation("Displays a download link with translation support."),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class DownloadLinkTranslatedFormatter extends FileFormatterBase {
  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $original_node = $items->getParent()->getValue();

    //collect files from all translations
    foreach($original_node->getTranslationLanguages() as $langcode => $tran) {
      /** @var $node \Drupal\node\Entity\Node */
      $node = $original_node->getTranslation($langcode);
      /** @var $file \Drupal\file\Entity\File*/
      $file = $node->get($items->getName())->entity;

      if (!empty($file)) {
        $url = $file->createFileUrl();
        $mime = $file->getMimeType();
        $size = format_size($file->getSize());

        $title = $node->getTitle();

        // We prepare a clean filename for the HTML 5 download attribute,
        // so the file is saved with a user-friendly name
        //$file_name = $file->getFileName();
        if (!empty($file->display_name)) {
          $display_name = $file->display_name->value;
        }
        else {
          $display_name = $title;
        }
        $file_name = str_replace(" ", "-", $display_name);
        // @TODO: this does not seem to work with ?download=true file handler;
        // @TODO: need to investigate

        // translate language link into local language
        $language = t($tran->getName(), [], ['langcode' => $langcode]);

        $elements[] = [
          '#type' => 'pattern',
          '#id' => 'download_link',
          '#fields' => [
            'file_name' => $file_name,
            'file_title' => $title,
            'url' => $url,
            'mime' => $mime,
            'download_label' => $language,
            'langcode' => $langcode,
            'size' => $size,
            'target' => '_self'
          ],
        ];
      }
    }
    return $elements;
  }
}
