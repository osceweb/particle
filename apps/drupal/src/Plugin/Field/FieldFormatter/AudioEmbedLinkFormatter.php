<?php

namespace Drupal\particle\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of a SoundCloud iFrame
 * Uses the audio pattern
 *
 * @FieldFormatter(
 *   id = "osce__audio_embed_link",
 *   label = @Translation("Embedded Audio"),
 *   description = @Translation("Embeds a (SoundCloud) audio iFrame."),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class AudioEmbedLinkFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    parent::viewElements($items, $langcode);

    $elements = [];

    foreach ($items as $item) {
      $elements[] = [
        '#type' => 'pattern',
        '#id' => 'audio',
        '#fields' => [
          'embed' => $this->getEmbedCode($item->getUrl()),
        ],
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Embed a SoundCloud audio');
    return $summary;
  }

  /**
   * Get Video from Youtube URL
   * @param \Drupal\Core\Url $url
   * @return bool|mixed
   */
  protected function getEmbedCode(Url $url) {
    // Inspired by soundcloudfield module, see https://git.drupalcode.org/project/soundcloudfield/blob/8.x-1.x/src/Plugin/Field/FieldFormatter/SoundCloudDefaultFormatter.php#L201
    // @see SoundCloud oembed documentation: https://developers.soundcloud.com/docs/api/reference#oembed
    $oembed_endpoint = 'https://soundcloud.com/oembed';

    $encoded_url = urlencode($url->getUri());
    $oembed_url = $oembed_endpoint . '?iframe=true&url=' . $encoded_url;

    $oembed = simplexml_load_string(particle_curl_get($oembed_url));

    return $oembed->html;
  }
}
