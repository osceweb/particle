<?php

namespace Drupal\particle\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;
use Drupal\file\Plugin\Field\FieldType\FileItem;

/**
 * Plugin implementation of the 'file_download_link' formatter.
 * Uses the download_link pattern
 *
 * @FieldFormatter(
 *   id = "osce__download_link",
 *   label = @Translation("OSCE Download link"),
 *   description = @Translation("Displays a funky download link."),
 *   field_types = {
 *     "file",
 *     "image"
 *   }
 * )
 */
class DownloadLinkFormatter extends FileFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $node = $items->getParent()->getValue();

    $files = $this->getEntitiesToView($items, $langcode);
    /** @var  $file \Drupal\file_entity\Entity\FileEntity */
    foreach ($files as $delta => $file) {
      $url = $file->createFileUrl();
      $mime = $file->getMimeType();
      $size = format_size($file->getSize());

      // get display name from description
      if (isset($file->_referringItem) &&  $file->_referringItem instanceof FileItem && !empty($file->_referringItem->getValue()['description'])) {
        $display_name = $file->_referringItem->getValue()['description'];
      }
      // get label from DS label setting
      else if (!empty($this->thirdPartySettings['ds']['ft']['settings']['lb'])) {
        $display_name = $this->thirdPartySettings['ds']['ft']['settings']['lb'];
      }
      // get display name from file display name
      else if (!empty($file->display_name) && !empty($file->display_name->value)) {
        $display_name = $file->display_name->value;
      }
      else if (!empty($file->getFileName())) { // if display name is not set, get file anme
        $display_name = $file->getFileName();
      }
      else { // set a generic name otheriwse
        $display_name = t("Download file");
      }
      // in a normal case label and title should be the same as the display name
      $download_label = $display_name;
      $title = $display_name;

      // Show a static download label for images
      if (count($files) == 1 && $this->fieldDefinition->getType() == 'image') {
        $download_label = t("Download high resolution image");
      }

      // translate language link into local language
      $language = t(\Drupal::languageManager()->getCurrentLanguage()->getName(), array(), array('langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId()));

      $elements[] = [
        '#type' => 'pattern',
        '#id' => 'download_link',
        '#fields' => [
          'file_name' => str_replace(" ", "-",$display_name),
          'file_title' => $title,
          'url' => $url,
          'mime' => $mime,
          'download_label' => $download_label,
          'language' => $language,
          'langcode' => $langcode,
          'size' => $size,
          'target' => '_self'
        ],
      ];
    }

    return $elements;
  }
}
