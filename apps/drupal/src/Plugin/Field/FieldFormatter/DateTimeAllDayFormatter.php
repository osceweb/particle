<?php

namespace Drupal\particle\Plugin\Field\FieldFormatter;

use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeDefaultFormatter;

/**
 * Plugin implementation of the 'Default' formatter for 'datetime' fields.
 *
 * @FieldFormatter(
 *   id = "osce_datetime_all_day",
 *   label = @Translation("All day"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class DateTimeAllDayFormatter extends DateTimeDefaultFormatter {

  /**
   * {@inheritdoc}
   */
  protected function formatDate($date) {
    $formatted_date = parent::formatDate($date);
    if ($formatted_date == '00' || $formatted_date == '00:00' || $formatted_date == '00:00:00') {
      return t('(All day)');
    }
    else {
      return $formatted_date;
    }
  }

}
