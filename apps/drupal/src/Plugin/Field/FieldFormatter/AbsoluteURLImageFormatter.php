<?php

namespace Drupal\particle\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'image_url' formatter.
 *
 * @FieldFormatter(
 *   id = "osce_image_absolute_url",
 *   label = @Translation("Image with absolute URL"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class AbsoluteURLImageFormatter extends ImageFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings() + [
        'width' => '100%',
        'height' => '',
      ];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);

    unset($element['image_link']);

    $element['width'] = [
      '#title' => t('Width'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('width') ?: 'auto',
      '#empty_option' => t('auto'),
      '#required' => true,
      '#size' => 10,
    ];

    $element['height'] = [
      '#title' => t('Height'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('height') ?: 'auto',
      '#empty_option' => 'auto',
      '#size' => 10,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    if ($this->getSetting('width')) {
      $summary[] = t('Width: ' . $this->getSetting('width'));
    }
    if ($this->getSetting('height')) {
      $summary[] = t('Height: ' . $this->getSetting('height'));
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    if (empty($images = $this->getEntitiesToView($items, $langcode))) {
      // Early opt-out if the field is empty.
      return $elements;
    }

    /** @var \Drupal\image\ImageStyleInterface $image_style */
    $image_style = $this->imageStyleStorage->load($this->getSetting('image_style'));

    // Collect cache tags to be added for each item in the field.
    $base_cache_tags = [];
    if (!empty($image_style_setting)) {
      $image_style = $this->imageStyleStorage->load($image_style_setting);
      $base_cache_tags = $image_style->getCacheTags();
    }

    /** @var \Drupal\file\FileInterface[] $images */
    foreach ($images as $delta => $image) {
      $image_uri = $image->getFileUri();
      $url = $image_style ? $image_style->buildUrl($image_uri) : file_create_url($image_uri);

      // Add cacheability metadata from the image and image style.
      $cacheability = CacheableMetadata::createFromObject($image);
      if ($image_style) {
        $cacheability->addCacheableDependency(CacheableMetadata::createFromObject($image_style));
      }

      $cache_contexts[] = 'url.site';
      $cache_tags = Cache::mergeTags($base_cache_tags, $image->getCacheTags());

      /** @var \Drupal\image\Plugin\Field\FieldType\ImageItem $item */
      $item = $image->_referringItem;
      $item_attrs = array_intersect_key($item->getValue(), array_flip(['src', 'width', 'height', 'title', 'alt']));
      $item_attrs['src'] = $url;
      if ($this->getSetting('width')) {
        $item_attrs['width'] = $this->getSetting('width');
      }
      if ($this->getSetting('height')) {
        $item_attrs['height'] = $this->getSetting('height');
      }

      $attributes = new Attribute($item_attrs);
      $elements[$delta] = [
        '#theme' => 'osce_image',
        '#attributes' => $attributes,
        '#cache' => [
          'tags' => $cache_tags,
          'contexts' => $cache_contexts,
        ],
      ];

      $cacheability->applyTo($elements[$delta]);
    }
    return $elements;
  }

}
