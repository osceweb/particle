<?php

namespace Drupal\particle\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of a Youtube iFrame
 * Uses the video pattern
 *
 * @FieldFormatter(
 *   id = "osce__video_embed_link",
 *   label = @Translation("Embedded Video"),
 *   description = @Translation("Embeds a (Youtube) video iFrame."),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class VideoEmbedLinkFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    parent::viewElements($items, $langcode);

    $elements = [];

    foreach ($items as $item) {
      $elements[] = [
        '#type' => 'pattern',
        '#id' => 'video',
        '#fields' => [
          'video_id' => $this->extractVideoId($item->getUrl()),
        ],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Embed a Youtoube video');
    return $summary;
  }

  /**
   * Get Video from Youtube URL
   * @param \Drupal\Core\Url $url
   * @return bool|mixed
   */
  protected function extractVideoId(Url $url) {
    if (!empty($url->getUri()) &&
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url->getUri(), $match)) {
      return $match[1];
    }
    return FALSE;
  }
}
