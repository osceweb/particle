<?php
namespace Drupal\particle\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

/**
 * An example controller.
 */
class ViewModeDemo extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function view($entity_type, $view_mode) {
    $allowed_entity_types = [
      'node',
      'paragraph',
      'media',
    ];
    $allowed_view_modes = [
      'figure',
      'figure_only',
      'highlight',
      'highlight_figure',
      'teaser',
      'document',
      'diashow',
    ];

    $use_fixed_ids = [
      'node' => [
        'video_clip' => 118466,
      ],
    ];

    if (!in_array($entity_type, $allowed_entity_types)) {
      return;
    }
    if (!in_array($view_mode, $allowed_view_modes)) {
      return;
    }

    $view_mode_missing = [];
    $view_mode_present = [];
    $storage_types = [
      'node' => 'node_type',
      'paragraph' => 'paragraphs_type',
      'media' => 'media_type',
    ];
    $contentTypes = \Drupal::service('entity_type.manager')->getStorage($storage_types[$entity_type])->loadMultiple();
    foreach ($contentTypes as $type) {
      $entity_display = \Drupal::service('entity_display.repository');
      $view_modes = $entity_display->getViewModeOptionsByBundle($entity_type, $type->id());
      if (isset($view_modes[$view_mode])) {
        $view_mode_present[] = $type->id();
      }
      else {
        $view_mode_missing[] = $type->id();
      }
    }

    foreach ($view_mode_present as $bundle) {
      $nid = FALSE;
      if (isset($use_fixed_ids[$entity_type][$bundle])) {
        $nid = $use_fixed_ids[$entity_type][$bundle];
      }
      $demos[$bundle] = $this->renderEntityView($entity_type, $bundle, $view_mode, $nid);
    }

    $links = [];
    foreach ($allowed_entity_types as $link_entity_type) {
      foreach ($allowed_view_modes as $link_view_mode) {
        $links[$link_entity_type][$link_view_mode] = Link::createFromRoute(
          $link_view_mode,
          'particle.viewmodedemo', [
            'entity_type' => $link_entity_type,
            'view_mode' => $link_view_mode,
          ]);
      }
    }

    $missing_edit_links = [];
    $edit_view_mode_route = 'entity.entity_view_display.' . $entity_type . '.default';

    foreach ($view_mode_missing as $missing_bundle) {
      $missing_edit_links[] = Link::createFromRoute(
        $missing_bundle,
        $edit_view_mode_route, [
          $storage_types[$entity_type] => $missing_bundle,
        ]);
    }

    $build = [
      '#theme' => 'view_mode_demo',
      '#links' => $links,
      '#missing_bundles' => [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => $missing_edit_links,
      ],
      '#demos' => $demos,
      '#view_mode' => $view_mode,
      '#entity_type' => $entity_type,
    ];
    return $build;
  }

  /**
   * Render a entity in a specific view mode.
   *
   * @param string $entity_type
   *   The entity type to render.
   * @param string $bundle
   *   The bundle type to render.
   * @param string $view_mode
   *   The view mode to render.
   * @param false|int $id
   *   The entity id. If empty a random entity is picked.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *   Thrown if the entity type doesn't exist.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   *   Thrown if the storage handler couldn't be loaded.
   *
   * @return array
   *   Returns a render array of a rendered entity.
   */
  private function renderEntityView($entity_type, $bundle, $view_mode, $id = FALSE) {
    $demo = ['#markup' => '<p>No content is present</p>'];

    $bundle_field = \Drupal::entityTypeManager()
      ->getDefinition($entity_type)
      ->getKey('bundle');

    if (!$id) {
      $ids = \Drupal::entityQuery($entity_type)
        ->condition($bundle_field, $bundle)
        ->addTag('sort_by_random')
        ->condition('status', 1)
        ->range(0, 1)
        ->execute();
      $id = reset($ids);
    }

    if ($id) {
      $view_builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
      $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
      if ($entity = $storage->load($id)) {
        $demo = $view_builder->view($entity, $view_mode);
      }
    }

    return $demo;
  }
}
