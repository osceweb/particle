<?php
$data = [
  'items' => [
    1 => [
      'label' => 'Activities',
      'children' => [
        [
          'label' => 'Arms control',
          'link' => '#',
        ],
        [
          'label' => 'Border management',
          'link' => '#',
        ],
        [
          'label' => 'Combating human trafficking',
          'link' => '#',
        ],
        [
          'label' => 'Conflict resolution and prevention',
          'link' => '#',
        ],
        [
          'label' => 'Countering terrorism',
          'link' => '#',
        ],
        [
          'label' => 'Cyber/ICT Security',
          'link' => '#',
        ],
        [
          'label' => 'Democratization',
          'link' => '#',
        ],
        [
          'label' => 'Economic activities',
          'link' => '#',
        ],
        [
          'label' => 'Gender equality',
          'link' => '#',
        ],
        [
          'label' => 'Good governance',
          'link' => '#',
        ],
        [
          'label' => 'Human rights',
          'link' => '#',
        ],
        [
          'label' => 'Media freedom and development',
          'link' => '#',
        ],
        [
          'label' => 'Migration',
          'link' => '#',
        ],
        [
          'label' => 'Minority rights',
          'link' => '#',
        ],
        [
          'label' => 'Policing',
          'link' => '#',
        ],
        [
          'label' => 'Reform and co-operation in the security sector',
          'link' => '#',
        ],
        [
          'label' => 'Roma and Sinti',
          'link' => '#',
        ],
        [
          'label' => 'Rule of law',
          'link' => '#',
        ],
        [
          'label' => 'Tolerance and non-discrimination',
          'link' => '#',
        ],
        [
          'label' => 'Youth',
          'link' => '#',
        ],
      ],
    ],
    2 => [
      'label' => 'On the ground',
      'children' => [
        [
          'label' => 'Presence in Albania',
          'link' => '#',
        ],
        [
          'label' => 'Mission to Bosnia and Herzegovina',
          'link' => '#',
        ],
        [
          'label' => 'Mission in Kosovo',
          'link' => '#',
        ],
        [
          'label' => 'Mission to Montenegro',
          'link' => '#',
        ],
        [
          'label' => 'Mission to Serbia',
          'link' => '#',
        ],
        [
          'label' => 'Mission to Skopje',
          'link' => '#',
        ],
        [
          'label' => 'Mission to Molodova',
          'link' => '#',
        ],
        [
          'label' => 'Project Co-ordinator in Ukraine',
          'link' => '#',
        ],
        [
          'label' => 'Special monitoring mission to Ukraine',
          'link' => '#',
        ],
        [
          'label' => 'Observer mission at the Russian Checkpoints Gukovo and Donetsk',
          'link' => '#',
        ],
        [
          'label' => 'Personal Representative of the Chairperson-In-Office on the Conflict Dealt With By the OSCE Minsk Conference',
          'link' => '#',
        ],
        [
          'label' => 'Centre in Ashgabat',
          'link' => '#',
        ],
        [
          'label' => 'Programme Office in Nur-Sultan',
          'link' => '#',
        ],
        [
          'label' => 'Programme Office in Bishkek',
          'link' => '#',
        ],
        [
          'label' => 'Programme Office in Dushanbe',
          'link' => '#',
        ],
        [
          'label' => 'Project Co-ordinator in Uzbekistan',
          'link' => '#',
        ],
        [
          'label' => 'Closed field operations',
          'link' => '#',
        ],
      ],
    ],
    3 => [
      'label' => 'Institutions & Structures',
      'children' => [
        [
          'label' => 'Parliamentary Assembly',
          'link' => '#',
        ],
        [
          'label' => 'High Commissioner on National Minorities',
          'link' => '#',
        ],
        [
          'label' => 'Office for Democratic Institutions and Human Rights',
          'link' => '#',
        ],
        [
          'label' => 'Representative on Freedom of the Media',
          'link' => '#',
        ],
        [
          'label' => 'Court of Conciliation and Arbitration',
          'link' => '#',
        ],
        [
          'label' => 'Economic and Environmental Forum',
          'link' => '#',
        ],
        [
          'label' => 'Minsk Group',
          'link' => '#',
        ],
        [
          'label' => 'Secretariat',
          'link' => '#',
        ],
      ],
    ],
//    6 => [
//      'label' => 'Search',
//      'link' => '#4',
//    ],
  ],
  // Enables the search field in the main menu.
  'show_search' => TRUE,

  'menu_items_footer' => [
    ['label' => 'Home', 'link' => 'https://www.osce.org'],
    ['label' => 'Employment', 'link' => 'https://jobs.osce.org'],
    ['label' => 'Procurement', 'link' => 'https://procurement.osce.org'],
    ['label' => 'Contacts', 'link' => 'https://www.osce.org/contacts'],
    ['label' => 'Terms of use', 'link' => '#'],
  ],
  'menu_items_meta' => [
    ['label' => 'Press Centre', 'link' => 'https://www.osce.org'],
    ['label' => 'Resources', 'link' => 'https://jobs.osce.org'],
  ],

  'home_latest_news' => [
    1 => [
      'teaser_text' => 'Ensure availability and sustainable management of water and sanitation for all Water is a strategic resource and an essential element of national and regional security. Water scarcity, lack of...',
      'teaser_url' => '#',
      'teaser_location' => FALSE,
      'teaser_date' => FALSE,
    ],
    2 => [
      'teaser_text' => 'Ensure access to affordable, reliable, sustainable and modern energy for all Energy is central to nearly every major challenge and opportunity the world faces today. Be it for food production,...',
      'teaser_url' => '#',
    ],
    3 => [
      'teaser_text' => 'Make cities and human settlements inclusive, safe, resilient and sustainable  - Cities are hubs for ideas, commerce, culture, science, productivity and social development. With the number of people...',
      'teaser_url' => '#',
    ],
  ],

  'taglinks' => [
    1 => [
      'label' => 'Arms control',
      'color' => 'white',
      'link' => '#',
    ],
    2 => [
      'label' => 'Border management and security',
      'color' => 'white',
      'link' => '#',
    ],
    3 => [
      'label' => 'Combating human trafficking',
      'color' => 'white',
      'link' => '#',
    ],
    4 => [
      'label' => 'Countering terrorism',
      'color' => 'white',
      'link' => '#',
    ],
    5 => [
      'label' => 'Conflict prevention and resolution',
      'color' => 'white',
      'link' => '#',
    ],
    6 => [
      'label' => 'Cyber/ICT Security',
      'color' => 'white',
      'link' => '#',
    ],
    7 => [
      'label' => 'Democratization',
      'color' => 'white',
      'link' => '#',
    ],
    8 => [
      'label' => 'Economic activities',
      'color' => 'white',
      'link' => '#',
    ],
    9 => [
      'label' => 'Education',
      'color' => 'white',
      'link' => '#',
    ],
    10 => [
      'label' => 'Elections',
      'color' => 'white',
      'link' => '#',
    ],
    11 => [
      'label' => 'Environmental activities',
      'color' => 'white',
      'link' => '#',
    ],
    12 => [
      'label' => 'Gender equality',
      'color' => 'white',
      'link' => '#',
    ],
    13 => [
      'label' => 'Good governance',
      'color' => 'white',
      'link' => '#',
    ],
    14 => [
      'label' => 'Human rights',
      'color' => 'white',
      'link' => '#',
    ],
    15 => [
      'label' => 'Media freedom and development',
      'color' => 'white',
      'link' => '#',
    ],
    16 => [
      'label' => 'Migration',
      'color' => 'white',
      'link' => '#',
    ],
    17 => [
      'label' => 'Minority rights',
      'color' => 'white',
      'link' => '#',
    ],
    18 => [
      'label' => 'Policing',
      'color' => 'white',
      'link' => '#',
    ],
    19 => [
      'label' => 'Reform and co-operation in the security sector',
      'color' => 'white',
      'link' => '#',
    ],
    20 => [
      'label' => 'Roma and Sinti',
      'color' => 'white',
      'link' => '#',
    ],
    21 => [
      'label' => 'Rule of law',
      'color' => 'white',
      'link' => '#',
    ],
    22 => [
      'label' => 'Tolerance and non-discrimination',
      'color' => 'white',
      'link' => '#',
    ],
    23 => [
      'label' => 'Youth',
      'color' => 'white',
      'link' => '#',
    ],
  ],
];
