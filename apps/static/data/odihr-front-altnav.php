<?php
$data = [
  'items' => [
    1 => [
      'label' => 'Home',
      'link' => '#1',
    ],
    2 => [
      'label' => 'About ODIHR',
      'children' => [
        [
          'label' => 'Director',
          'link' => '#',
        ],
        [
          'label' => 'Mandate',
          'link' => '#',
        ],
        [
          'label' => 'Human Dimension',
          'link' => '#',
        ],
        [
          'label' => 'Former ODIHR directors',
          'link' => '#',
        ],
      ],
    ],
    3 => [
      'label' => 'Activities',
      'link' => '#3',
      'children' => [
        [
          'label' => 'Tolerance',
          'link' => '#',
        ],
        [
          'label' => 'Elections',
          'link' => '#',
        ],
        [
          'label' => 'Human rights',
          'link' => '#',
        ],
        [
          'label' => 'Democracy',
          'link' => '#',
        ],
        [
          'label' => 'Gender equality',
          'link' => '#',
        ],
        [
          'label' => 'Roma & Sinti',
          'link' => '#',
        ],
      ],
    ],
    4 => [
      'label' => 'Elections',
      'link' => '#4',
    ],
    5 => [
      'label' => 'Resources',
      'link' => '#5',
      'children' => [
        [
          'label' => 'News',
          'link' => '#',
        ],
        [
          'label' => 'Press releases',
          'link' => '#',
        ],
        [
          'label' => 'Stories',
          'link' => '#',
        ],
        [
          'label' => 'Publications',
          'link' => '#',
        ],
        [
          'label' => 'Documents',
          'link' => '#',
        ],
        [
          'label' => 'Multimedia',
          'link' => '#',
        ],
        [
          'label' => 'Events',
          'link' => '#',
        ],
      ],
    ],
    6 => [
      'label' => 'Search',
      'link' => '#4',
    ]
  ],

  'follow_source_networks' => [
    [
      'name' => 'Facebook',
      'class' => 'facebook',
      'url' => "#",
    ],
    [
      'name' => 'Twitter',
      'class' => 'twitter',
      'url' => "#",
    ],
    [
      'name' => 'LinkedIn',
      'class' => 'linkedin',
      'url' => "#",
    ],
  ],

  'menu_items_footer' => [
    ['label' => 'ODIHR Home', 'link' => '#', 'active'=> TRUE],
    ['label' => 'OSCE Main Website', 'link' => 'https://www.osce.org'],
    ['label' => 'Employment', 'link' => 'https://jobs.osce.org'],
    ['label' => 'Procurement', 'link' => 'https://procurement.osce.org'],
    ['label' => 'Contacts', 'link' => 'https://www.osce.org/contacts'],
    ['label' => 'Terms of use', 'link' => '#'],
  ]
];
