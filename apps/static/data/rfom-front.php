<?php
$data = [
  'site_logo' => 'rfom',
  'site_follow_source' => 'RFOM',
  'items' => [
    1 => [
      'label' => 'Home',
      'link' => '#1',
    ],
    2 => [
      'label' => 'Who we are',
      'children' => [
        [
          'label' => 'Our mandate',
          'link' => '#',
        ],
        [
          'label' => 'Representative',
          'link' => '#',
        ],
      ],
    ],
    3 => [
      'label' => 'What we do',
      'link' => '#3',
      'children' => [
        ['label' => 'Access to information', 'link' => '#'],
        ['label' => 'Decriminalization of defamation', 'link' => '#'],
        ['label' => 'Digital switchover', 'link' => '#'],
        ['label' => 'Hate speech', 'link' => '#'],
        ['label' => 'Impact of Artificial Intelligence', 'link' => '#'],
        ['label' => 'Media freedom on the internet', 'link' => '#'],
        ['label' => 'Media laws', 'link' => '#'],
        ['label' => 'Media pluralism', 'link' => '#'],
        ['label' => 'Media self-regulation', 'link' => '#'],
        ['label' => 'Safety of journalists', 'link' => '#'],
        ['label' => 'Safety of Female Journalists Online', 'link' => '#'],
      ],
    ],
    5 => [
      'label' => 'Resources',
      'link' => '#5',
      'children' => [
        ['label' => 'Publications', 'link' => '#'],
        ['label' => 'Documents', 'link' => '#'],
        ['label' => 'Media', 'link' => '#'],
        ['label' => 'Events', 'link' => '#'],
      ],
    ],
  ],
  'show_search' => TRUE,

  'follow_source_networks' => [
    [
      'name' => 'Facebook',
      'class' => 'facebook',
      'url' => "#",
    ],
    [
      'name' => 'Twitter',
      'class' => 'twitter',
      'url' => "#",
    ],
    [
      'name' => 'LinkedIn',
      'class' => 'linkedin',
      'url' => "#",
    ],
  ],

  'menu_items_footer' => [
    ['label' => 'RFOM Home', 'link' => '#', 'active'=> TRUE],
    ['label' => 'OSCE Main Website', 'link' => 'https://www.osce.org'],
    ['label' => 'Employment', 'link' => 'https://jobs.osce.org'],
    ['label' => 'Procurement', 'link' => 'https://procurement.osce.org'],
    ['label' => 'Contacts', 'link' => 'https://www.osce.org/contacts'],
    ['label' => 'Terms of use', 'link' => '#'],
  ],

  'home_latest_news' => [
    1 => [
      'teaser_text' => 'Ensure availability and sustainable management of water and sanitation for all Water is a strategic resource and an essential element of national and regional security. Water scarcity, lack of...',
      'teaser_url' => '#',
      'teaser_location' => FALSE,
      'teaser_date' => FALSE,
    ],
    2 => [
      'teaser_text' => 'Ensure access to affordable, reliable, sustainable and modern energy for all Energy is central to nearly every major challenge and opportunity the world faces today. Be it for food production,...',
      'teaser_url' => '#',
    ],
    3 => [
      'teaser_text' => 'Make cities and human settlements inclusive, safe, resilient and sustainable  - Cities are hubs for ideas, commerce, culture, science, productivity and social development. With the number of people...',
      'teaser_url' => '#',
    ],
  ],


];
