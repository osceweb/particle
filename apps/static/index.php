<?php
error_reporting(E_ALL & ~ E_NOTICE);
require_once '../pl/pattern-lab/vendor/autoload.php';
use PatternLab\PatternEngine\Twig\TwigUtil;
use PatternLab\Config;
use Symfony\Component\Yaml\Parser;

$template_root = '../../source/default/_patterns';
$template_groups = [
  'protons' => '00-protons',
  'elements' => '01-elements',
  'objects' => '02-objects',
  'containers' => '03-containers',
  'composites' => '04-composites',
  'layouts' => '05-layouts',
  'pages' => '06-pages',
];

$loader = new \Twig\Loader\FilesystemLoader($template_root);
$loader->addPath('./templates', 'mockups');
foreach ($template_groups as $template_group => $template_path) {
  $loader->addPath($template_root . '/' . $template_path, $template_group);
}

$twig = new \Twig\Environment($loader, ['debug' => true, 'autoescape' => false]);
$twig->addExtension(new \Twig\Extension\DebugExtension());
// Patternlab config.
Config::setOption('sourceDir', '../../source/default/');
// customize Twig
TwigUtil::setInstance($twig);

TwigUtil::loadFilters();
TwigUtil::loadFunctions();
TwigUtil::loadTags();
TwigUtil::loadTests();
TwigUtil::loadDateFormats();
TwigUtil::loadDebug();
TwigUtil::loadMacros();

$yaml = new Parser();
$vars = $yaml->parse(file_get_contents('../../source/default/_data/data.yml'));

$page = 'mockups';
$pages = [
  'main-front' => 'Main Frontpage',
  'odihr-front' => 'ODIHR Frontpage',
  'odihr-front-altnav' => 'ODIHR Frontpage (Alternative Navigation)',
  'rfom-front' => 'RFOM Frontpage',
];
if ($_GET['page'] && in_array($_GET['page'], array_keys($pages))) {
  $page = $_GET['page'];
  $vars['page'] = $_GET['page'];
  $vars['page_label'] =$pages[$page];
}

$vars['pages'] = $pages;
$vars['paths']['assets'] = '../../dist/app-drupal/assets/';

if (file_exists('data/' . $page . '.php')) {
  require_once('data/' . $page . '.php');
  $vars = array_merge($vars, $data);
}


switch($page) {
  case 'mockups':
    $template = "@mockups/mockups.html.twig";
    break;
  default:
    $template = "@mockups/_html.twig.html";
}

echo $twig->render($template, $vars);
