---
el: 'site-top'
title: 'site-top'
---

`source/_patterns/layouts/site-top/_site-top.twig`

##Usage:

```
{% include '@layouts/site-top/_site-top.twig' %}
```

or
```
{% embed '@layouts/site-top/_site-top.twig' %}
      {% block block_site_top %}

      {% endblock block_site_top %}
    {% endembed %}
```
