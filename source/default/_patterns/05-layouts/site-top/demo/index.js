/**
 * Demo of siteTop. Pulls in siteTop assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'layouts/site-top';

// Import demo assets
import twig from './site-tops.twig';
import yaml from './site-tops.yml';
import markdown from './site-tops.md';

export default {
  twig,
  yaml,
  markdown,
};
