import { name } from '..';

test('site-top component is registered', () => {
  expect(name).toBe('siteTop');
});
