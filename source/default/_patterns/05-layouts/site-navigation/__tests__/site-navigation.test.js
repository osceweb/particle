import { name } from '..';

test('site-navigation component is registered', () => {
  expect(name).toBe('siteNavigation');
});
