/**
 * Demo of siteNavigation. Pulls in siteNavigation assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'layouts/site-navigation';

// Import demo assets
import twig from './site-navigations.twig';
import yaml from './site-navigations.yml';
import markdown from './site-navigations.md';

export default {
  twig,
  yaml,
  markdown,
};
