---
el: 'site-navigation'
title: 'site-navigation'
---

`source/_patterns/layouts/site-navigation/_site-navigation.twig`

##Usage:

```
{% include '@layouts/site-navigation/_site-navigation.twig' %}
```

or


```
{% include '@layouts/site-navigation/_site-navigation.twig' 
with {
varname: '',
}
%}
```
