import { name } from '..';

test('site-content component is registered', () => {
  expect(name).toBe('siteContent');
});
