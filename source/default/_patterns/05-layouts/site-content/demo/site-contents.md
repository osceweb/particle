---
el: 'site-content'
title: 'site-content'
---

`source/_patterns/layouts/site-content/_site-content.twig`

##Usage:

```
{% include '@layouts/site-content/_site-content.twig' %}
```

or

```
{% embed '@layouts/site-content/_site-content.twig' %}
  {% block block_site_content_bottom %}
     content sidebar bottom
  {% endblock block_site_content_bottom %}
  {% block block_site_content_footer %}
     content footer
  {% endblock block_site_content_footer %}
{% endembed %}
```
