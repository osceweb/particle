/**
 * Demo of siteContent. Pulls in siteContent assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'layouts/site-content';

// Import demo assets
import twig from './site-contents.twig';
import yaml from './site-contents.yml';
import markdown from './site-contents.md';

export default {
  twig,
  yaml,
  markdown,
};
