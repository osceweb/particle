/**
 * Demo of siteMultilingual. Pulls in siteMultilingual assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'layouts/site-multilingual';

// Import demo assets
import twig from './site-multilinguals.twig';
import yaml from './site-multilinguals.yml';
import markdown from './site-multilinguals.md';

export default {
  twig,
  yaml,
  markdown,
};
