---
el: 'site-multilingual'
title: 'site-multilingual'
---

`source/_patterns/layouts/site-multilingual/_site-multilingual.twig`

##Usage:

```
{% include '@layouts/site-multilingual/_site-multilingual.twig' %}
```

or
```
{% embed '@layouts/site-multilingual/_site-multilingual.twig' %}
      {% block block_site_top %}

      {% endblock block_site_top %}
    {% endembed %}
```
