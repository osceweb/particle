import { name } from '..';

test('site-multilingual component is registered', () => {
  expect(name).toBe('siteMultilingual');
});
