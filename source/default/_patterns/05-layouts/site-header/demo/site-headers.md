---
el: 'site-header'
title: 'site-header'
---

`source/_patterns/layouts/site-header/_site-header.twig`

##Usage:

```
{% include '@layouts/site-header/_site-header.twig' %}
```

or
```
{% embed '@layouts/site-header/_site-header.twig' %}
      {% block block_site_header %}

      {% endblock block_site_header %}
    {% endembed %}
```

