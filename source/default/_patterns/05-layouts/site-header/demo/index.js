/**
 * Demo of siteHeader. Pulls in siteHeader assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'layouts/site-header';

// Import demo assets
import twig from './site-headers.twig';
import yaml from './site-headers.yml';
import markdown from './site-headers.md';

export default {
  twig,
  yaml,
  markdown,
};
