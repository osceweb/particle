import { name } from '..';

test('site-header component is registered', () => {
  expect(name).toBe('siteHeader');
});
