/**
 * Footer
 */

// Module dependencies
import 'protons';

// Module styles
import './_footer.scss';

// Module template
import './footer.twig';

export const name = 'footer';

export function disable() {}

export function enable() {}

export default enable;
