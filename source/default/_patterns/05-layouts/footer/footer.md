---
el: '.footer'
title: 'Footer'
---

`source/_patterns/05-layouts/footer/footer.twigg`

##Usage:

```
{% include "@layouts/footer/footer.twig" %}
```

or

```
{% embed '@layouts/footer/footer.twig' %}
{% block footer_01 %}
  FOOTER 1
{% endblock footer_01 %}
{% block footer_02 %}
  FOOTER 2
{% endblock footer_02 %}

{% endembed %}
```
