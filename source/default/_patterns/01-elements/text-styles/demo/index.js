/**
 * Demo of textStyles. Pulls in textStyles assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './text-styless.twig';
import yaml from './text-styless.yml';

export default {
  twig,
  yaml,
};
