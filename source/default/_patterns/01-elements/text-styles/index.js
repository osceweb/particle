/**
 * textStyles
 */

// Module dependencies
import 'protons';

// Module styles
import './_text-styles.scss';

// Module template
import './_text-styles.twig';

export const name = 'textStyles';

export function disable() {}

export function enable() {}

export default enable;
