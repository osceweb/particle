import { name } from '..';

test('text-styles component is registered', () => {
  expect(name).toBe('text-styles');
});
