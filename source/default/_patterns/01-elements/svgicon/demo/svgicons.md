---
el: '.svgicons'
title: 'SVG Icons'
---

`source/_patterns/01-elements/svgicon/_svgicon.twig`

##Usage:

```
  {% include '@elements/svgicon/_svgicon.twig'
      with {
      svgname: 'SVG-name',
      }
      %}
```

SVG icon with class (F.E: 'bigger'):

```
  {% include '@elements/svgicon/_svgicon.twig'
      with {
      svgname: 'SVG-name',
      svg_class: 'yourclassname',
      }
      %}
```

SVG icon colored icon (F.E: 'light'):

```
  {% include '@elements/svgicon/_svgicon.twig'
      with {
      svgname: 'SVG-name',
      svgfill: 'light',
      }
      %}
```
