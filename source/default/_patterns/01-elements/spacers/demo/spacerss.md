---
el: 'spacers'
title: 'spacer'
---

`source/_patterns/elements/spacers/_spacers.twig`

##Usage:

```
{% include '@elements/spacers/_spacers.twig' %}
```

or


```
{% include '@elements/spacers/_spacers.twig' 
with {
 spacerclass_value: 'dashed',
}
%}
```

or


```
{% include '@elements/spacers/_spacers.twig' 
with {
 spacerclass_value: 'dotted',
}
%}
```

or


```
{% include '@elements/spacers/_spacers.twig' 
with {
 spacerclass_value: 'invisible',
}
%}
```
