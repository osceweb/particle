---
el: 'spacers-condensed-invisible'
title: 'spacer condensed invisible'
---

`source/_patterns/elements/spacers/_spacers-condensed.twig`

##Usage:


```
{% include '@elements/spacers/_spacers-condensed.twig' 
with {
 spacerclass_value: 'condensed invisible',
}
%}
```
