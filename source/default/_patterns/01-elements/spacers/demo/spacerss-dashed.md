---
el: 'spacers-dashed'
title: 'spacer dashed'
---

`source/_patterns/elements/spacers/_spacers.twig`

##Usage:


```
{% include '@elements/spacers/_spacers.twig' 
with {
 spacerclass_value: 'dashed',
}
%}
```
