---
el: 'spacers-condensed'
title: 'spacer condensed'
---

`source/_patterns/elements/spacers/_spacers-condensed.twig`

##Usage:


```
{% include '@elements/spacers/_spacers-condensed.twig' 
with {
 spacerclass_value: 'condensed',
}
%}
```
