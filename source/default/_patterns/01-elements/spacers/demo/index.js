/**
 * Demo of spacers. Pulls in spacers assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'elements/spacers';

// Import demo assets
import twig from './spacerss.twig';
import yaml from './spacerss.yml';
import markdown from './spacerss.md';

export default {
  twig,
  yaml,
  markdown,
};
