---
el: 'spacers-invisible'
title: 'spacer invisible'
---

`source/_patterns/elements/spacers/_spacers.twig`

##Usage:


```
{% include '@elements/spacers/_spacers.twig' 
with {
 spacerclass_value: 'invisible',
}
%}
```
