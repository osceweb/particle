---
el: 'spacers-dotted'
title: 'spacer dotted'
---

`source/_patterns/elements/spacers/_spacers.twig`

##Usage:

```
{% include '@elements/spacers/_spacers.twig' 
with {
 spacerclass_value: 'dotted',
}
%}
```

