import { name } from '..';

test('spacers component is registered', () => {
  expect(name).toBe('spacers');
});
