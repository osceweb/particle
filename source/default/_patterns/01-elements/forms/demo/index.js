/**
 * Demo of forms. Pulls in forms assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'elements/forms';

// Import demo assets
import twig from './formss.twig';
import yaml from './formss.yml';
import markdown from './formss.md';

export default {
  twig,
  yaml,
  markdown,
};
