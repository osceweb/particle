---
el: 'forms'
title: 'forms'
---

`source/_patterns/elements/forms/_forms.twig`

##Usage:

```
{% include '@elements/forms/_forms.twig' %}
```

or


```
{% include '@elements/forms/_forms.twig' 
with {
varname: '',
}
%}
```
