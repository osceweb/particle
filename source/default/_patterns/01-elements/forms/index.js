/**
 * forms
 */

import $ from 'jquery';
// Module dependencies
import 'protons';

// Module styles
import './_forms.scss';
import './checkbox/_checkbox.scss';
import './radio/_radio.scss';
import './select/_select.scss';

// Module template
import './_forms.twig';
import './checkbox/checkbox.twig';
import './checkbox/_checkbox-item.twig';
import './radio/radio.twig';
import './radio/_radio-item.twig';
import './select/select.twig';
import './select/_select-item.twig';
import './_fieldset.twig';
import './_form-element.twig';
import './_form-label.twig';
import './_input.twig';
import './_multi-value.twig';
import './_radios.twig';
import './_select-element.twig';
import './_textarea.twig';

export const name = 'forms';

export const defaults = {
  dummyClass: 'js-forms-exists',
};

/**
 * Components may need to run clean-up tasks if they are removed from DOM.
 *
 * @param {jQuery} $context - A piece of DOM
 * @param {Object} settings - Pertinent settings
 */
// eslint-disable-next-line no-unused-vars
export function disable($context, settings) {}

/**
 * Each component has a chance to run when its enable function is called. It is
 * given a piece of DOM ($context) and a settings object. We destructure our
 * component key off the settings object and provide an empty object fallback.
 * Incoming settings override default settings via Object.assign().
 *
 * @param {jQuery} $context - A piece of DOM
 * @param {Object} settings - Settings object
 */
export function enable($context, { forms = {} }) {
  // Find our component within the DOM
  const $forms = $('.forms', $context);
  // Bail if component does not exist
  if (!$forms.length) {
    return;
  }
  // Merge defaults with incoming settings
  const settings = Object.assign(defaults, forms);
  // An example of what could be done with this component
  $forms.addClass(settings.dummyClass);
}

export default enable;
