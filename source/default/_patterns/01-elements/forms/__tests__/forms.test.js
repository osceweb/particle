import { name } from '..';

test('forms component is registered', () => {
  expect(name).toBe('forms');
});
