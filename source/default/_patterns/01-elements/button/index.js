/**
 * button
 */

// Module dependencies

// Module styles
import './_button.scss';

// Module template
import './_button.twig';

export const name = 'button';

export function disable() {}

export function enable() {}

export default enable;
