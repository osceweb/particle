---
el: '.buttons'
title: 'Button'
---

`source/_patterns/01-elements/button/_button.twig`

##Usage:

```
{% include '@elements/button/_button.twig'
with {
button_txt: 'your button text',
}
%}
```

Button with font icon:

```
{% include '@elements/button/_button.twig'
with {
button_txt: 'your button text',
button_class_name: 'your button class name (default: button)',
button_icon_name: 'icon_name',
}
%}
```

Button with svg icon:

```
  {% include '@elements/button/_button.twig'
    with {
      button_txt: 'your button text',
      button_class_name: 'your button class name (default: button)',
      button_icon_name: 'icon_name',
      svg_icon: 'true',
    }
    %}
```

Button with colored svg icon:

```
  {% include '@elements/button/_button.twig'
    with {
      button_txt: 'your button text',
      button_class_name: 'your button class name (default: button)',
      button_icon_name: 'icon_name',
      svg_icon: 'true',
      button_svg_icon_color: 'light',
    }
    %}
```
