---
el: '.sections'
title: 'Section'
---

`source/_patterns/03-containers/section/_section.twig`

##Usage:

```
{% include '@containers/section/_section.twig' %}
```

{# Section with no top border, title, header or label #}
```
{% embed '@containers/section/_section.twig'
  with {
  section_border: false
} %}
{% endembed %}
```