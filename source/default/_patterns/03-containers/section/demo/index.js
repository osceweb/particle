/**
 * Demo of section. Pulls in section assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './sections.twig';
import yaml from './sections.yml';

export default {
  twig,
  yaml,
};
