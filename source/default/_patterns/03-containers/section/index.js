/**
 * section
 */

// Module dependencies
import 'protons';

// Module styles
import './_section.scss';

// Module template
import './_section.twig';
import './section.ui_patterns.yml';

export const name = 'section';

export function disable() {}

export function enable() {}

export default enable;
