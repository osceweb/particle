import { name } from '..';

test('table component is registered', () => {
  expect(name).toBe('table');
});
