/**
 * Demo of table. Pulls in table assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './tables.twig';
import yaml from './tables.yml';

export default {
  twig,
  yaml,
};
