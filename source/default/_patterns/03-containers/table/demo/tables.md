---
el: '.tables'
title: 'Table'
---

`source/_patterns/03-containers/table/_table.twig`

##Usage:

```
{% include '@containers/table/_table.twig' %}
```

or

```
{% extends '@containers/table/_table.twig' %}
  {% block table_content %}
      {% include '@containers/teaser/_teaser.twig' %}
  {% endblock %}
```
