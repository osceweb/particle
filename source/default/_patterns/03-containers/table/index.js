/**
 * table
 */

// Module dependencies
import 'protons';

// Module styles
import './_table.scss';

// Module template
import './_table.twig';

export const name = 'table';

export function disable() {}

export function enable() {}

export default enable;
