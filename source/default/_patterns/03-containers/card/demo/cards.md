---
el: 'card'
title: 'card'
---

`source/_patterns/containers/card/_card.twig`

##Usage:

```
{% include '@containers/card/_card.twig' %}
```

or


```
{% include '@containers/card/_card.twig'
    with {
        card_image: '<img src="example.jpg" alt="" /> />',
        card_title: 'My title',
        card_description: '<p>This is my description</p>',
        card_url: 'https://www.example.com'
    }
%}
```
