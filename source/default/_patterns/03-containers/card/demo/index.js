/**
 * Demo of card. Pulls in card assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'containers/card';

// Import demo assets
import twig from './cards.twig';
import yaml from './cards.yml';
import markdown from './cards.md';

export default {
  twig,
  yaml,
  markdown,
};
