---
el: '.documents'
title: 'Document'
---

`source/_patterns/03-containers/document/_document.twig`

##Usage:

```
{% include @containers/document/_document.twig %}
```

or

```
{% include '@containers/document/_document.twig'
with {
    cover: item.cover,
    document_description: item.description,
    document_url: item.document_url,
    document_link_type: 'link type',
    document_link_translation: 'translations',
}
%}
```

or

```
{% embed '@containers/gallery/_gallery.twig'
with {
    items: document_items,
}
%}
    {% block block_gallery_content %}
        {% include '@containers/document/_document.twig'
            with {
              cover: item.cover,
              document_description: item.document_description,
              document_title: item.document_title,
              document_url: item.document_url,
            }
            %}
    {% endblock %}
{% endembed %}
```

