/**
 * Demo of document. Pulls in document assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './documents.twig';
import yaml from './documents.yml';

export default {
  twig,
  yaml,
};
