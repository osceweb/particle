/**
 * document
 */

// Module dependencies
import 'protons';

// Module styles
import './_document.scss';

// Module template
import './_document.twig';

export const name = 'document';

export function disable() {}

export function enable() {}

export default enable;
