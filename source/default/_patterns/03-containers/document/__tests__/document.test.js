import { name } from '..';

test('document component is registered', () => {
  expect(name).toBe('document');
});
