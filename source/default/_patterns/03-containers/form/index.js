/**
 * form
 */

// Module dependencies
// import 'protons';

// Module styles
import './_form.scss';

// Module template
import './_form.twig';

export const name = 'form';

export function disable() {}

export function enable() {}

export default enable;
