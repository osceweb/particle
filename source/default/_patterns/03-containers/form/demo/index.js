/**
 * Demo of form. Pulls in form assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './forms.twig';
import yaml from './forms.yml';

export default {
  twig,
  yaml,
};
