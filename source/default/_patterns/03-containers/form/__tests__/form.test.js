import { name } from '..';

test('form component is registered', () => {
  expect(name).toBe('form');
});
