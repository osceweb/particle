import { name } from '..';

test('teaserlist component is registered', () => {
  expect(name).toBe('teaserlist');
});
