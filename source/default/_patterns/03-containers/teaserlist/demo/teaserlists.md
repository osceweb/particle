---
el: 'teaserlist'
title: 'teaserlist'
---

`source/_patterns/containers/teaserlist/_teaserlist.twig`

##Usage:

```
{% include '@containers/teaserlist/_teaserlist.twig' %}
```

or
```
{% embed '@containers/teaserlist/_teaserlist.twig'
with {
  items: teaser_items,
}
%}
  {% block block_teaser_list %}
    {% embed '@containers/teaser/_teaser.twig'
    with {
      teaser_image: item.teaser_image,
    }
    %}

      {% block block_teaser_image %}
      {{ teaser_image }}
      {% endblock %}

      {% block block_teaser_main %}
        Main content
      {% endblock %}

    {% endembed %}
  {% endblock %}
{% endembed %}
```

or F.E "Latest News" ()

{% embed '@containers/teaserlist/_teaserlist.twig'
  with {
  items: home_latest_news,
}
%}
  {% block block_teaser_list %}
    {% embed '@containers/teaser/_teaser.twig'
      with {
      teaser_image: item.teaser_image,
      item.show_more_link: '',
    }
    %}
      {% block block_teaser_image %}
        {{ teaser_image }}
      {% endblock %}
      {% block block_teaser_main %}
        {% if item.teaser_text | render %}
          <div class="teaser__text">
            {{ item.teaser_text | t }}
          </div>
        {% endif %}
        {% if item.teaser_location or item.teaser_date %}
          <div class="location">
          {% if item.teaser_location | render %}
            {{ item.teaser_location }},
          {% endif %}
          {% if item.teaser_date | render %}
            {{ item.teaser_date }}
          {% endif %}
          </div>
      {% endif %}
      {% endblock %}
    {% endembed %}
  {% endblock %}
{% endembed %}
         

