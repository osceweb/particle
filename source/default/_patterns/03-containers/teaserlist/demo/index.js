/**
 * Demo of teaserlist. Pulls in teaserlist assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'containers/teaserlist';

// Import demo assets
import twig from './teaserlists.twig';
import yaml from './teaserlists.yml';
import markdown from './teaserlists.md';

export default {
  twig,
  yaml,
  markdown,
};
