import { name } from '..';

test('gallery component is registered', () => {
  expect(name).toBe('gallery');
});
