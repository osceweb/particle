/**
 * gallery2cols
 */

// Module dependencies
import 'protons';

// Module styles
import './_gallery2cols.scss';

// Module template
import './_gallery2cols.twig';

export const name = 'gallery2cols';

export function disable() {}

export function enable() {}

export default enable;
