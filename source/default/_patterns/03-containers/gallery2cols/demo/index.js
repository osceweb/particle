/**
 * Demo of gallery2cols. Pulls in gallery2cols assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './gallerys2cols.twig';
import yaml from './gallerys2cols.yml';

export default {
  twig,
  yaml,
};
