---
el: '.gallerys2cols'
title: '2 cols gallery'
---

`source/_patterns/03-containers/gallery2cols/_gallery2cols.twig`

##Usage:

```
{% include '@containers/gallery2cols/_gallery2cols.twig' %}
```
or
```
{% include '@containers/gallery2cols/_gallery2cols.twig'
with {
    items: youritems,
}
%}
```
or
```
   {% embed '@containers/gallery2cols/_gallery2cols.twig' %}
          {% block block_gallery_content %}
            {% include '@containers/document/_document.twig'
              with {
              cover: '<img src="https://www.osce.org/files/imagecache/50_thumb/f/images/hires/1/7/400244_0.png?1539854402" alt="This volume provides practical guidance and helpful background for both policy makers and practitioners who are working to advance civil-society-led P/CVERLT initiatives in the South-Eastern European region." title="This volume provides practical guidance and helpful background for both policy makers and practitioners who are working to advance civil-society-led P/CVERLT initiatives in the South-Eastern European region." class="imagecache imagecache-50_thumb" width="75" height="100">',
              description: 'The Role of Civil Society in Preventing and Countering Violent Extremism and Radicalization that Lead to Terrorism: A Focus on South-Eastern Europe',
            }
            %}
          {% endblock %}
        {% endembed %}
```