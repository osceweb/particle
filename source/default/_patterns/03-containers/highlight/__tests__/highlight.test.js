import { name } from '..';

test('highlight component is registered', () => {
  expect(name).toBe('highlight');
});
