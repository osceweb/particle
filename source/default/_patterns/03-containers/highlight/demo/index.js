/**
 * Demo of highlight. Pulls in highlight assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'containers/highlight';

// Import demo assets
import twig from './highlights.twig';
import yaml from './highlights.yml';
import markdown from './highlights.md';

export default {
  twig,
  yaml,
  markdown,
};
