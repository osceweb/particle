---
el: 'highlight'
title: 'highlight'
---

`source/_patterns/containers/highlight/_highlight.twig`

##Usage:

```
{% include '@containers/highlight/_highlight.twig' %}
```

or


```
{% include '@containers/highlight/_highlight.twig'
    with {
    highlight_image: item.highlight_image,
    highlight_description: item.highlight_description,
    highlight_title: item.highlight_title,
    highlight_url: item.highlight_url,
    highlight_icon: 'camera'
} %}
```
