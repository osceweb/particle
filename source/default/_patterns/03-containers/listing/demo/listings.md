---
el: '.listings'
title: 'Listing'
---

`source/_patterns/03-containers/listing/_listing.twig`

##Usage:

```
{% include '@containers/listing/_listing.twig' %}
```

or

```
{% embed '@containers/listing/_listing.twig' %}
  {% block listing_content %}
      {% include '@containers/teaser/_teaser.twig' %}
  {% endblock %}
{% endembed %}
```

or 
```
{% include '@containers/_listing.twig'
  with {
  items: {1: '...', 2: '...'},
  format: 'normal'
  }
%}
```