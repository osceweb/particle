/**
 * Demo of listing. Pulls in listing assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './listings.twig';
import yaml from './listings.yml';

export default {
  twig,
  yaml,
};
