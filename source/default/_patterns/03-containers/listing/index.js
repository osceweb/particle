/**
 * listing
 */

// Module dependencies
import 'protons';

// Module styles
import './_listing.scss';

// Module template
import './_listing.twig';

export const name = 'listing';

export function disable() {}

export function enable() {}

export default enable;
