import { name } from '..';

test('listing component is registered', () => {
  expect(name).toBe('listing');
});
