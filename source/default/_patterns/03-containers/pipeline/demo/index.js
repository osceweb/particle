/**
 * Demo of pipeline. Pulls in pipeline assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './pipelines.twig';
import yaml from './pipelines.yml';

export default {
  twig,
  yaml,
};
