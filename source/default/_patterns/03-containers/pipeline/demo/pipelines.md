---
el: '.pipelines'
title: 'pipeline'
---

`source/_patterns/03-containers/pipeline/_pipeline.twig`

##Usage:

```
{% include '@containers/pipeline/_pipeline.twig' %}
```

or

```
{% embed '@containers/pipeline/_pipeline.twig' %}
  {% block pipe_content %}
    {% include '@containers/teaser/_teaser.twig' %}
  {% endblock %}
{% endembed %}
```

or

```
{% extends '@containers/pipeline/_pipeline.twig' %}
  {% block pipe_content %}
      {% include '@containers/teaser/_teaser.twig' %}
  {% endblock %}
```
