/**
 * pipeline
 */

// Module dependencies
import 'protons';

// Module styles
import './_pipeline.scss';

// Module template
import './_pipeline.twig';

export const name = 'pipeline';

export function disable() {}

export function enable() {}

export default enable;
