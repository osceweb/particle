import { name } from '..';

test('pipeline component is registered', () => {
  expect(name).toBe('pipeline');
});
