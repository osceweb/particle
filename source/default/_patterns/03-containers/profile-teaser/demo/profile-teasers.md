---
el: 'profile-teaser'
title: 'profile-teaser'
---

`source/_patterns/containers/profile-teaser/_profile-teaser.twig`

##Usage:

```
{% include '@containers/profile-teaser/_profile-teaser.twig' %}
```

or


```
{% include '@containers/profile-teaser/_profile-teaser.twig' 
with {
varname: '',
}
%}
```
