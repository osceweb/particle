/**
 * Demo of profileTeaser. Pulls in profileTeaser assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'containers/profile-teaser';

// Import demo assets
import twig from './profile-teasers.twig';
import yaml from './profile-teasers.yml';
import markdown from './profile-teasers.md';

export default {
  twig,
  yaml,
  markdown,
};
