import { name } from '..';

test('profile-teaser component is registered', () => {
  expect(name).toBe('profileTeaser');
});
