import { name } from '..';

test('bar component is registered', () => {
  expect(name).toBe('bar');
});
