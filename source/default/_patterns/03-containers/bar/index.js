/**
 * bar
 */

// Module dependencies
import 'protons';

// Module styles
import './_bar.scss';

// Module template
import './_bar.twig';
import './bar.ui_patterns.yml';

export const name = 'bar';

export function disable() {}

export function enable() {}

export default enable;
