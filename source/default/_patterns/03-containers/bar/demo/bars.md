---
el: '.bars'
title: 'Bar'
---

`source/_patterns/03-containers/bar/_bar.twig`

##Usage:

```
{% include '@containers/bar/_bar.twig' %}
```

or

```
{% embed '@containers/bar/_bar.twig' %}
          {% block block_bar_content %}
            {% include '@protons/demo/_f-placeholder.twig'
              with {
              'placeholder_height': '40px',
              'placeholder_width': '120px',
              'placeholder_content': '01 01 dummy.',
            }
            %}
            YOUR CONTENT
          {% endblock %}
        {% endembed %}
```

or

```
{% 'include @containers/bar/_bar.twig'
with {
bar_content: 'your bar content',
}
%}
```
or with extra classes f.E.: 'for a kind of 2 column bar'
```
          {% embed '@containers/bar/_bar.twig'
          with {
            bar_classes_value: 'bar bar-2cols',
          }
          %}
            {% block block_bar_content %}
              <div>
                {% include '@composites/metadata-list/_metadata-list.twig'
                  with {
                  items: pressrelease_meta_data_type,
                  meta_data_list: 'type',
                }
                %}
              </div>
              <div>
                {% include '@objects/social-media/_social-media-share.twig' %}
              </div>
              <div>
                {% include '@objects/language-switcher/_language-switcher.twig' %}
              </div>
            {% endblock block_bar_content %}
          {% endembed %}

```
