---
el: '.bar'
title: 'Bar'
---

`source/_patterns/03-containers/bar/_bar.twig`

##Usage:

```
{% 'include @containers/bar/_bar.twig' %}
```

or

```
{% 'include @containers/bar/_bar.twig'
with {
  bar_content_classes_value: 'bar__content your_class',
}
%}
```

or

```
{% embed '@containers/bar/_bar.twig'
with {
  bar_content_classes_value: 'bar__content container',
}
%}
  {% block block_bar_content %}
    YOURCOONTENT
  {% endblock %}
{% endembed %}
```
