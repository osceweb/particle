---
el: 'sdg-icons'
title: 'sdg-icons'
---

`source/_patterns/containers/sdg-icons/_sdg-icons.twig`

##Usage:

```
{% include '@containers/sdg-icons/_sdg-icons.twig' %}
```

or


```
{% include '@containers/sdg-icons/_sdg-icons.twig' 
with {
items: your_items, 
sdg_more_info_url: 'your url', 
}
%}
```
