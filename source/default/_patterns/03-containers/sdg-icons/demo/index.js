/**
 * Demo of sdgIcons. Pulls in sdgIcons assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'containers/sdg-icons';

// Import demo assets
import twig from './sdg-iconss.twig';
import yaml from './sdg-iconss.yml';
import markdown from './sdg-iconss.md';

export default {
  twig,
  yaml,
  markdown,
};
