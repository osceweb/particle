/**
 * bento
 */

// Module dependencies
import 'protons';

// Module styles
import './_bento.scss';

// Module template
import './_bento.twig';

export const name = 'bento';

export function disable() {}

export function enable() {}

export default enable;
