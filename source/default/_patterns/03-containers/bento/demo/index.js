/**
 * Demo of bento. Pulls in bento assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './bentos.twig';
import yaml from './bentos.yml';

export default {
  twig,
  yaml,
};
