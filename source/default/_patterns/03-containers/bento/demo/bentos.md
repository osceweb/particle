---
el: '.bentos'
title: 'Bento'
---

`source/_patterns/03-containers/source/_patterns/03-containers/bento/_bento.twig`

##Usage:

```
{% include @containers/bento/_bento.twig %}
```

or

```
{% embed '@containers/bento/_bento.twig' %}
  {% block block_bento_top %}
    Top
  {% endblock %}
  {% block block_bento_main %}
    Main
  {% endblock %}
  {% block block_bento_bottom %}
  Bottom
  {% endblock %}
{% endembed %}
%}
```
