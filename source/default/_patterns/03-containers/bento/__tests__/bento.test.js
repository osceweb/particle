import { name } from '..';

test('bento component is registered', () => {
  expect(name).toBe('bento');
});
