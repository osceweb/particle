/**
 * adminBox
 */

// Module dependencies
import 'protons';

// Module styles
import './_admin-box.scss';

// Module template
import './_admin-box.twig';

export const name = 'adminBox';

export function disable() {}

export function enable() {}

export default enable;
