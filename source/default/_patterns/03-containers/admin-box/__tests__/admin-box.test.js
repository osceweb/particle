import { name } from '..';

test('admin-box component is registered', () => {
  expect(name).toBe('admin-box');
});
