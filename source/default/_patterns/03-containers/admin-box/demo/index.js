/**
 * Demo of adminBox. Pulls in adminBox assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './admin-boxs.twig';
import yaml from './admin-boxs.yml';

export default {
  twig,
  yaml,
};
