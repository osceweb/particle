import { name } from '..';

test('box component is registered', () => {
  expect(name).toBe('box');
});
