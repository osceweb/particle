/**
 * Demo of box. Pulls in box assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './boxs.twig';
import yaml from './boxs.yml';

export default {
  twig,
  yaml,
};
