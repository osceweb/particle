---
el: '.boxs'
title: 'Box'
---

`source/_patterns/03-containers/box/_box.twig`

##Box types (box_type):
- blue
- dark
- darkdavriant
- shaded
- shaded-light
- todo

##Optional
### box label types (box_label_type):
- bleeding
### collapsable box options (box_label_type):
- bleeding

##Usage:

```
{% include @containers/box/_box.twig %}
```

or

```
{% embed '@containers/box/_box.twig'
with {
  box_label: 'Label dummy text',
  box_label_type: 'bleeding',
  box_header:'Your header',
  box_header_subtitle: 'subtitle',
  box_type: 'blue' 
}
%}
  {% block box_main %}
   Main
  {% endblock %}
{% endembed %}
```
or
display contents as "list"(inline)
```
{% embed '@containers/box/_box.twig'
with {
  box_label: 'Label dummy text',
  box_content_class: 'box__content--list',
}
%}
  {% block box_main %}
   Main
  {% endblock %}
{% endembed %}
```
