import $ from 'jquery';
import _ from 'underscore';
import Backbone from 'backbone';
import enquire from 'enquire.js';

export const Model = Backbone.Model.extend({
  defaults: {
    enabled: 'yes',
    collapsed: 'no',
    duration: '250',
    enable: false,
    disable: false,
    collapse: false,
    expand: false,
  },
  initialize() {
    this.handlers = {
      enable: { match: () => this.enable(), unmatch: () => this.disable() },
      disable: { match: () => this.disable(), unmatch: () => this.enable() },
      collapse: { match: () => this.collapse(), unmatch: () => this.expand() },
      expand: { match: () => this.expand(), unmatch: () => this.collapse() },
    };
    _.each(['enable', 'disable', 'collapse', 'expand'], action => {
      this.on(`change:${action}`, _.partial(this.registerQuery, action));
    });
  },
  registerQuery(action, model, value) {
    if (model.previous(action)) {
      enquire.unregister(model.previous(action), this.handlers[action]);
    }
    if (value) {
      enquire.register(value, this.handlers[action]);
    }
  },
  collapse() {
    this.set('collapsed', 'yes');
  },
  expand() {
    this.set('collapsed', 'no');
  },
  enable() {
    this.set('enabled', 'yes');
  },
  disable() {
    this.set('enabled', 'no');
  },
  isEnabled() {
    return _.contains(['yes', 'true', '1'], this.get('enabled'));
  },
  isCollapsed() {
    return _.contains(['yes', 'true', '1'], this.get('collapsed'));
  },
  getDuration() {
    return parseInt(this.get('duration'), 10);
  },
  validate(attributes) {
    if (!/yes|no|true|false|0|1/i.test(attributes.enabled)) {
      return `'${attributes.enabled}' is not a valid for attribute 'enabled'.`;
    }
    if (!/yes|no|true|false|0|1+/i.test(attributes.collapsed)) {
      return `${
        attributes.collapsed
      } is not a valid for attribute 'collapsed'.`;
    }
    return false;
  },
});

export const Component = Backbone.View.extend({
  expand() {
    return true;
  },
  collapse() {
    return true;
  },
});

export const Wrapper = Backbone.View.extend({
  events: {
    attached: 'attachComponent',
    detached: 'detachComponent',
    'collapsible-toggled': 'toggle',
  },

  initialize() {
    this.components = [];
    this.animating = false;
    this.model = new Model();
    console.log('in here!');
  },

  attachComponent(event) {
    if (event.target.backbone instanceof Component) {
      event.stopPropagation();
      if (!_.contains(this.components, event.target)) {
        /* eslint-disable-next-line no-param-reassign */
        event.target.backbone.model = this.model;

        this.model.on(
          'change',
          event.target.backbone.render,
          event.target.backbone
        );
        event.target.backbone.render();
        this.components.push(event.target);
      }
    }
  },

  detachComponent(event) {
    if (event.target.view instanceof Component) {
      event.stopPropagation();
      this.model.off('change', event.target.backbone.render);
      this.components = _.filter(this.components, c => c !== event.target);
    }
  },

  toggle(event) {
    event.stopPropagation();
    if (this.animating) {
      return;
    }
    this.animating = true;
    (this.model.isCollapsed() ? this.expand() : this.collapse()).then(() => {
      this.model.set('collapsed', this.model.isCollapsed() ? 'no' : 'yes');
      this.animating = false;
    });
  },

  animate(state) {
    return Promise.all(
      _.chain(this.components)
        .map(_.property('backbone'))
        .invoke(state ? 'collapse' : 'expand')
        .filter(promise => promise instanceof Promise)
        .value()
    );
  },

  expand() {
    return this.animate(false);
  },

  collapse() {
    return this.animate(true);
  },
});

export const Content = Component.extend(
  {
    initialize() {
      this.$wrapper = $('<div class="clearfix"></div>');
      for (let i = this.el.children.length; i >= 0; i -= 1) {
        this.$wrapper.prepend(this.el.children[i]);
      }
      this.$el.append(this.$wrapper);
    },

    render() {
      this.$el.css({
        opacity: this.model.isCollapsed() && this.model.isEnabled() ? 0 : 1,
        height: this.model.isCollapsed() && this.model.isEnabled() ? 0 : 'auto',
      });
      return this;
    },

    collapse() {
      return $(this.el).hide();
    },

    expand() {
      $(this.el).show();
    },

    height() {
      return this.$wrapper.outerHeight(true);
    },
  },
  { tag: 'collapsible-content' }
);

export const Toggle = Component.extend({
  events: {
    click: 'toggle',
  },

  toggle(event) {
    event.stopPropagation();
    event.preventDefault();
    this.$el.trigger('collapsible-toggled');
  },
});

export const Burger = Toggle.extend({
  initialize() {
    this.$el.append(`
      <button class="hamburger hamburger--elastic" type="button">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>
    `);
    this.$burger = this.$el.find('.hamburger');
  },
  render() {
    if (!this.model.isEnabled()) {
      this.$el.hide();
      return this;
    }
    this.$el.show();
    return this;
  },
  collapse() {
    this.$burger.removeClass('is-active');
  },
  expand() {
    this.$burger.addClass('is-active');
  },
});

export const Icon = Toggle.extend({
  initialize() {
    this.$el.append(`<div><span></span><span></span></div>`);
    this.$el.find('span').css('background', this.$el.css('color'));
    this.vertical = this.$el.find('span:first-child').first();
    this.horizontal = this.$el.find('span:last-child').first();
  },

  render() {
    if (!this.model.isEnabled()) {
      this.$el.hide();
      return this;
    }
    this.$el.show();
    $(this.vertical).css({
      transform: this.model.isCollapsed() ? 'rotate(90deg)' : 'rotate(0deg)',
    });
    $(this.horizontal).css({
      transform: this.model.isCollapsed() ? 'rotate(180deg)' : 'rotate(0deg)',
    });
    return this;
  },

  collapse() {},

  expand() {},
});
