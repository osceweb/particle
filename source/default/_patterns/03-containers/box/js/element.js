import $ from 'jquery';
import _ from 'underscore';
/* eslint-disable no-underscore-dangle */
/* eslint-disable func-names */

export const register = (tag, constructor = null) => {
  if (!_.isString(tag)) {
    throw new Error('Please provide a tag name for your custom element.');
  }

  if (customElements.get(tag) !== undefined) {
    // Avoid duplicate registration.
    return;
  }

  customElements.define(
    tag,
    class extends HTMLElement {
      constructor() {
        super();
        this.__bb_constructor = constructor;
        this.__bb_tag = tag;
        this.backbone = null;

        if (this.__bb_constructor) {
          this.backbone = new this.__bb_constructor({
            el: this,
          });

          this.__bb_model =
            _.result(this.backbone, 'elementModel') ||
            _.result(this.backbone, 'model');
          if (this.__bb_model) {
            const attributes = _.chain(this.attributes)
              .map(attribute => _.object([[attribute.name, attribute.value]]))
              .reduce((memo, current) => _.extend({}, memo, current), {})
              .value();

            this.__bb_model.on(
              'change',
              model => {
                this.backbone.$el.attr(
                  _.omit(model.changed, ['id', 'class', 'style'])
                );
              },
              this
            );

            this.__bb_model.set(attributes, { validate: true });
          }
        }
      }

      attributeChangedCallback(attribute, oldValue, newValue) {
        if (_.contains(['id', 'class', 'style'], attribute)) {
          return;
        }
        if (typeof this !== 'undefined' && this.__bb_model) {
          this.__bb_model.set(_.object([[attribute, newValue]]), {
            validate: true,
          });
        }
      }

      connectedCallback() {
        $(this).trigger('attached');
      }

      disconnectedCallback() {
        $(this).trigger('detached');
      }
    }
  );
};
export default register;
/* eslint-enable no-underscore-dangle */
/* eslint-enable func-names */
