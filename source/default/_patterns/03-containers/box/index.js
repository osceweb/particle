/**
 * box
 */

// Module dependencies
import 'protons';

// Module styles
import './_box.scss';

// Module template
import './_box.twig';
import './box.ui_patterns.yml';
import * as Element from './js/element';
import * as Collapsible from './js/collapsible';
import 'document-register-element';

export const name = 'box';

export function disable() {}

export function enable() {
  Element.register('collapsible-wrapper', Collapsible.Wrapper);
  Element.register('collapsible-content', Collapsible.Content);
  Element.register('collapsible-toggle', Collapsible.Toggle);
  Element.register('collapsible-icon', Collapsible.Icon);
  Element.register('collapsible-burger', Collapsible.Burger);
}

export default enable;
