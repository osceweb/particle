/**
 * Demo of gallery. Pulls in gallery assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './gallerys.twig';
import yaml from './gallerys.yml';

export default {
  twig,
  yaml,
};
