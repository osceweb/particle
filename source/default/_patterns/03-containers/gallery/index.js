/**
 * gallery
 */

// Module dependencies
import 'protons';

// Module styles
import './_gallery.scss';

// Module template
import './_gallery.twig';

export const name = 'gallery';

export function disable() {}

export function enable() {}

export default enable;
