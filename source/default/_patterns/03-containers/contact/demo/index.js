/**
 * Demo of contact. Pulls in contact assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'containers/contact';

// Import demo assets
import twig from './contacts.twig';
import yaml from './contacts.yml';
import markdown from './contacts.md';

export default {
  twig,
  yaml,
  markdown,
};
