import { name } from '..';

test('contact component is registered', () => {
  expect(name).toBe('contact');
});
