/**
 * figureJustified
 */

import $ from 'jquery';

// Module dependencies
import 'protons';

// Module styles
import './_figure-justified.scss';

// Module template
import './_figure-justified.twig';

export const name = 'figureJustified';

export const defaults = {
  dummyClass: 'js-figureJustified-exists',
};

/**
 * Components may need to run clean-up tasks if they are removed from DOM.
 *
 * @param {jQuery} $context - A piece of DOM
 * @param {Object} settings - Pertinent settings
 */
// eslint-disable-next-line no-unused-vars
export function disable($context, settings) {}

/**
 * Each component has a chance to run when its enable function is called. It is
 * given a piece of DOM ($context) and a settings object. We destructure our
 * component key off the settings object and provide an empty object fallback.
 * Incoming settings override default settings via Object.assign().
 *
 * @param {jQuery} $context - A piece of DOM
 * @param {Object} settings - Settings object
 */
// eslint-disable-next-line no-unused-vars
export function enable($context, { figureJustified = {} }) {
  $('.figure-justified--link').each((index, element) => {
    const link = $(element);
    if (link.data('url') !== link.attr('href')) {
      link.attr('href', link.data('url'));
    }
  });
  $('.figure-justified--detail').hide();
}

export default enable;
