import { name } from '..';

test('figure-justified component is registered', () => {
  expect(name).toBe('figureJustified');
});
