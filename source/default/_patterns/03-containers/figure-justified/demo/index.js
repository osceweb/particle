/**
 * Demo of figureJustified. Pulls in figureJustified assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'containers/figure-justified';

// Import demo assets
import twig from './figure-justifieds.twig';
import yaml from './figure-justifieds.yml';
import markdown from './figure-justifieds.md';

export default {
  twig,
  yaml,
  markdown,
};
