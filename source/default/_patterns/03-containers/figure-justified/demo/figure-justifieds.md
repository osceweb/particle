---
el: 'figure-justified'
title: 'figure-justified'
---

`source/_patterns/containers/figure-justified/_figure-justified.twig`

##Usage:
### Parameters:
- figure_url: The Url to the image that will be shown in the lightbox
- figure_fallback_url: The url to the node/media element the item belongs to. This will be linked in the lightbox and is a fallback if JS is disabled.
- figure_rel: Set a lightbox gallery rel id to group multiple images to a gallery
- figure_thumbnail: The html to render an image. Attention: If it contains Drupals contextual links from a normal field output, justified gallery will fail.
- figure_caption: The caption for the image

```
{% include '@containers/_figure-justified.twig'
      with {
      figure_url: paths.assets ~ "/900x540r.png",
      figure_fallback_url: "/node/1",
      figure_rel: "1",
      figure_thumbnail: '<img src="' ~ paths.assets ~ "/900x540r.png" ~ '" width="300" height="150" />',
      figure_caption: "<p>I am a caption with <strong>HTML</strong></p>",
    } %}
```
