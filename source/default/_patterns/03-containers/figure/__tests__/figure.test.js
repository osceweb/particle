import { name } from '..';

test('figure component is registered', () => {
  expect(name).toBe('figure');
});
