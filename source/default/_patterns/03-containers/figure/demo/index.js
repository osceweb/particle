/**
 * Demo of figure. Pulls in figure assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './figures.twig';
import yaml from './figures.yml';

export default {
  twig,
  yaml,
};
