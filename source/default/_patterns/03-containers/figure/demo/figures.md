---
el: '.figures'
title: 'Figure'
---

`source/_patterns/03-containers/figure/_figure.twig`

##Usage:

```
{% include '@containers/figure/_figure.twig'  with {
    figure_image: figure_image
    figure_caption: figure_caption
    figure_url: figure_url    
}%}
```

### Use of icons
A figure can have an icon overlay, just set `figure_icon` to a valid icon name defined in svgicons for example:

- movie
- archive
- camera
- folder
- …

See [SVGIcons](link.elements-svgicons) for more icons.

or

```
    {% include '@containers/figure/_figure.twig' with {        
        figure_title: "OSCE Chairperson-in-Office Miroslav Lajčák visits Ukraine",
        figure_description: "OSCE Chairperson-in-Office and Minister of Foreign and European Affairs of Slovakia Miroslav Lajčák travelled to Ukraine on 15-16 January 2019.",
        figure_icon: 'camera'
    }%}
```
