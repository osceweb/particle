/**
 * figure
 */

// Module dependencies
import 'protons';

// Module styles
import './_figure.scss';

// Module template
import './_figure.twig';

export const name = 'figure';

export function disable() {}

export function enable() {}

export default enable;
