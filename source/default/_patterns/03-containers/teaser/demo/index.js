/**
 * Demo of teaser. Pulls in teaser assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './teasers.twig';
import yaml from './teasers.yml';

export default {
  twig,
  yaml,
};
