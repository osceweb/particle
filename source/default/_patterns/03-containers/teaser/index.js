/**
 * teaser
 */

// Module dependencies
import 'protons';

// Module styles
import './_teaser.scss';

// Module template
import './_teaser.twig';

export const name = 'teaser';

export function disable() {}

export function enable() {}

export default enable;
