import { name } from '..';

test('teaser component is registered', () => {
  expect(name).toBe('teaser');
});
