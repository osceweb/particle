---
el: '.content-2colss'
title: 'Content 2 columns'
---

`source/_patterns/03-containers/content-2cols/_content-2cols.twig`

##Usage:

```
{% include '@containers/content-2cols/_content-2cols.twig' %}
```
or
```
{% embed '@containers/content-2cols/_content-2cols.twig' %}
{% block block_content_2cols_image %}
  {{ content_2cols_image }}
{% endblock block_content_2cols_image %}

{% block block_content_2cols_main %}
  {{ content_2cols_main }}
{% endblock block_content_2cols_main %}
{% endembed %}
```
