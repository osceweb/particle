/**
 * Demo of content-2cols. Pulls in content-2cols assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './content-2colss.twig';
import yaml from './content-2colss.yml';

export default {
  twig,
  yaml,
};
