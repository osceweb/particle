import { name } from '..';

test('content-2cols component is registered', () => {
  expect(name).toBe('content-2cols');
});
