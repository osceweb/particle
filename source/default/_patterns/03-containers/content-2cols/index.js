/**
 * content-2cols
 */

// Module dependencies
import 'protons';

// Module styles
import './_content-2cols.scss';

// Module template
import './_content-2cols.twig';

export const name = 'content-2cols';

export function disable() {}

export function enable() {}

export default enable;
