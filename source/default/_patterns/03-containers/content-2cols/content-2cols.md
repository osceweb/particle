---
el: '.content-2cols'
title: 'Content 2columns'
---

`source/_patterns/03-containers/content-2cols/_content-2cols.twig`

##Usage:

```
{% include '@containers/content-2cols/_content-2cols.twig' %}
```
