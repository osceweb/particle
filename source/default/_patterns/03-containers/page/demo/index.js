/**
 * Demo of page. Pulls in page assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './pages.twig';
import yaml from './pages.yml';

export default {
  twig,
  yaml,
};
