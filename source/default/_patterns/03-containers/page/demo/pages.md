---
el: '.pages'
title: 'Page'
---

`source/_patterns/03-containers/page/_page.twig`

##Usage:

```
{% include '@containers/page/_page.twig' %}
```

or

```
{% include '@containers/page/_page.twig'
with {
page_toolbar: 'toolbar',
page_header: 'header',
page_navigation:  'navi',
page_breadcrumbs:  'breadcrumbs',
page_hero:  'hero',
page_content:  'content',
page_footer:  'footer',
}
%}
```

or

```
{% extends '@containers/page/_page.twig' %}
    {% block block_page_toolbar %}
        "TOOLBAR"
    {% endblock %}
    {% block block_page_header %}
        "HEADER"
    {% endblock %}
    {% block block_page_breadcrumbs %}
        "BREADCRUMBS"
    {% endblock %}
    {% block block_page_navigation %}
        "NAVIGATION"
    {% endblock %}
    {% block block_page_hero %}
        "HERO"
    {% endblock %}
    {% block block_page_content %}
        "CONTENT"
    {% endblock %}
    {% block block_page_footer %}
        "FOOTER"
    {% endblock %}
```
