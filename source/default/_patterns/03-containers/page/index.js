/**
 * page
 */

// Module dependencies
import 'protons';

// Module styles
import './_page.scss';

// Module template
import './_page.twig';

export const name = 'page';

export function disable() {}

export function enable() {}

export default enable;
