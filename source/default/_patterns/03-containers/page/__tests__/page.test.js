import { name } from '..';

test('row component is registered', () => {
  expect(name).toBe('row');
});
