/**
 * Demo of metadataList. Pulls in metadataList assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'composites/metadata-list';

// Import demo assets
import twig from './metadata-lists.twig';
import yaml from './metadata-lists.yml';
import markdown from './metadata-lists.md';

export default {
  twig,
  yaml,
  markdown,
};
