---
el: 'metadata-list'
title: 'metadata-list'
---

`source/_patterns/composites/metadata-list/_metadata-list.twig`

##Usage:

```
{% include '@composites/metadata-list/_metadata-list.twig' %}
```

or

```
{% include '@composites/metadata-list/_metadata-list.twig' 
with {
items: meta_data_items,
}
%}
```
