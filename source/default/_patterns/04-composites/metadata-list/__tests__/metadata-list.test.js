import { name } from '..';

test('metadata-list component is registered', () => {
  expect(name).toBe('metadataList');
});
