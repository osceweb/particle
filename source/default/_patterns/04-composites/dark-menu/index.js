/**
 * darkMenu
 */

// Module dependencies
import 'protons';

// Module styles
import './_dark-menu.scss';

// Module template
import './_dark-menu.twig';

export const name = 'darkMenu';

export function disable() {}

export function enable() {}

export default enable;
