/**
 * Demo of darkMenu. Pulls in darkMenu assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './dark-menus.twig';
import yaml from './dark-menus.yml';

export default {
  twig,
  yaml,
};
