---
el: '.dark-menus'
title: 'Dark Menu'
---

`source/_patterns/04-composites/dark-menu/_dark-menu.twig`

##Usage:

```
{% include '@composites/dark-menu/_dark-menu.twig' %}
```

or

```
{% include '@composites/dark-menu/_dark-menu.twig'

with {
items: your_menu_items,
}
%}
```
