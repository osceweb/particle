import { name } from '..';

test('dark-menu component is registered', () => {
  expect(name).toBe('dark-menu');
});
