/**
 * Demo of subscribeBox. Pulls in subscribeBox assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './subscribe-boxs.twig';
import yaml from './subscribe-boxs.yml';

export default {
  twig,
  yaml,
};
