import { name } from '..';

test('subscribe-box component is registered', () => {
  expect(name).toBe('subscribe-box');
});
