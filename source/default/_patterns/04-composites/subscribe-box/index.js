/**
 * subscribeBox
 */

// Module dependencies
import 'protons';

// Module styles
import './_subscribe-box.scss';

// Module template
import './_subscribe-box.twig';

export const name = 'subscribeBox';

export function disable() {}

export function enable() {}

export default enable;
