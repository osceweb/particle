/**
 * Demo of loginBox. Pulls in loginBox assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './login-boxs.twig';
import yaml from './login-boxs.yml';

export default {
  twig,
  yaml,
};
