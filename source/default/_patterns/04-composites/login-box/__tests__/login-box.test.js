import { name } from '..';

test('login-box component is registered', () => {
  expect(name).toBe('login-box');
});
