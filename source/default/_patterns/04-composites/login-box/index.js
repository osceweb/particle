/**
 * loginBox
 */

// Module dependencies
import 'protons';

// Module styles
import './_login-box.scss';

// Module template
import './_login-box.twig';

export const name = 'loginBox';

export function disable() {}

export function enable() {}

export default enable;
