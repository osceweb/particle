/**
 * Demo of definitionCompact. Pulls in definitionCompact assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './definition-compacts.twig';
import yaml from './definition-compacts.yml';

export default {
  twig,
  yaml,
};
