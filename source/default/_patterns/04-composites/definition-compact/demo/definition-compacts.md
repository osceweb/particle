el: '.definition-compacts'
title: 'Definition Compact'

---

`source/_patterns/04-composites/definition-compact/_definition-compact.twig`

##Usage:

```
{% include '@composites/definition-compact/_definition-compact.twig' %}
```

or

```
{% include '@composites/definition-compact/_definition-compact.twig'
with {
items: youritems,
}
%}
```
