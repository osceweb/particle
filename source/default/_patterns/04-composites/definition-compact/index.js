/**
 * definitionCompact
 */

// Module dependencies
import 'protons';

// Module styles
import './_definition-compact.scss';

// Module template
import './_definition-compact.twig';

export const name = 'definitionCompact';

export function disable() {}

export function enable() {}

export default enable;
