import { name } from '..';

test('definition-compact component is registered', () => {
  expect(name).toBe('definition-compact');
});
