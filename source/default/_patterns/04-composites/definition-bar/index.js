/**
 * definitionBar
 */

// Module dependencies
import 'protons';

// Module styles
import './_definition-bar.scss';

// Module template
import './_definition-bar.twig';

export const name = 'definitionBar';

export function disable() {}

export function enable() {}

export default enable;
