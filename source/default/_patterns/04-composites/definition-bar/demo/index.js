/**
 * Demo of definitionBar. Pulls in definitionBar assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './definition-bars.twig';
import yaml from './definition-bars.yml';

export default {
  twig,
  yaml,
};
