el: '.definition-bars'
title: 'Definition Bar'

---

`source/_patterns/04-composites/definition-bar/_definition-bar.twig`

##Usage:

```
{% include '@composites/definition-bar/_definition-bar.twig' %}
```

or

```
{% include '@composites/definition-bar/_definition-bar.twig'
with {
items: youritems,
}
%}
```
