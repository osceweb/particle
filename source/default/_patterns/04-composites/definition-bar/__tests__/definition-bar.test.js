import { name } from '..';

test('definition-bar component is registered', () => {
  expect(name).toBe('definition-bar');
});
