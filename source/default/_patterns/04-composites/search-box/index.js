/**
 * searchBox
 */

// Module dependencies
import 'protons';

// Module styles
import './_search-box.scss';

// Module template
import './_search-box.twig';

export const name = 'searchBox';

export function disable() {}

export function enable() {}

export default enable;
