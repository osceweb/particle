/**
 * Demo of searchBox. Pulls in searchBox assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './search-boxs.twig';
import yaml from './search-boxs.yml';

export default {
  twig,
  yaml,
};
