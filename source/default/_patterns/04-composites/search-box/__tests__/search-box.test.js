import { name } from '..';

test('search-box component is registered', () => {
  expect(name).toBe('search-box');
});
