/**
 * Demo of reportDownload. Pulls in reportDownload assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'composites/report-download';

// Import demo assets
import twig from './report-downloads.twig';
import yaml from './report-downloads.yml';
import markdown from './report-downloads.md';

export default {
  twig,
  yaml,
  markdown,
};
