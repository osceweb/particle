---
el: 'report-download'
title: 'report-download'
---

`source/_patterns/composites/report-download/_report-download.twig`

##Usage:

```
{% include '@composites/report-download/_report-download.twig' %}
```

or


```
{% include '@composites/report-download/_report-download.twig' 
with {
varname: '',
}
%}
```
