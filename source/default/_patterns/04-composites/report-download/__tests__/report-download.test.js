import { name } from '..';

test('report-download component is registered', () => {
  expect(name).toBe('reportDownload');
});
