import { name } from '..';

test('application-form component is registered', () => {
  expect(name).toBe('application-form');
});
