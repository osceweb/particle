/**
 * applicationForm
 */

// Module dependencies
import 'protons';

// Module styles
import './_application-form.scss';

// Module template
import './_application-form.twig';

export const name = 'applicationForm';

export function disable() {}

export function enable() {}

export default enable;
