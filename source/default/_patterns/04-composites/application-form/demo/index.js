/**
 * Demo of applicationForm. Pulls in applicationForm assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './application-forms.twig';
import yaml from './application-forms.yml';

export default {
  twig,
  yaml,
};
