el: '.application-forms'
title: 'Application Form'

---

`source/_patterns/04-composites/application-form/_application-form.twig`

##Usage:

```
{% include '@composites/application-form/_application-form.twig' %}
```

or

```
{% include '@composites/application-formt/_application-form.twig'
with {
items: youritems,
}
%}
```
