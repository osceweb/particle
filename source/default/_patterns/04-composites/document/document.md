---
el: 'document'
title: 'single document example'
---

`source/_patterns/composites/document/_document.twig`

##Usage:

```
{% include '@containers/document/_document.twig'
  with {
  cover: '<img src="https://www.osce.org/files/imagecache/50_thumb/f/images/hires/c/2/116945.png" alt="OSCE Special Monitoring Mission to Ukraine (SMM): Table of ceasefire violations as of 22 January 2019" class="imagecache imagecache-50_thumb" width="75" height="100">',
  document_url: '#',
  document_description: 'Description text',
  document_title: 'Document title',
  document_link_type: 'extra info',
}
%}
```
or

```
{% include '@containers/document/_document.twig'
  with {
  cover: item.cover,
  document_description: item.document_description,
  document_title: item.document_title,
  document_url: item.document_url,
  document_link_translation: item.document_link_translation,
  document_link_type: item.document_link_type,
}
%}
```
