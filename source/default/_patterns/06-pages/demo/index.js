/**
 * Full Page Demos
 *
 * This file is NOT imported by design-system.js, but is included as part of
 * particle/apps/pl/index.js
 */

import $ from 'jquery';

// Ensure all assets required by demos are present.
import 'protons';
import 'layouts/site-container.twig';
import 'layouts/basic-page';
import 'layouts/footer';
import 'layouts/site-content';
import 'layouts/site-header';
import 'layouts/site-multilingual';
import 'layouts/site-navigation';
import 'layouts/site-top';
// Demo templates.
import './document.twig';
import './document.yml';
import './homepage.twig';
import './homepage.yml';
import './image.twig';
import './image.yml';
import './pressrelease.twig';
import './pressrelease.yml';
import './smmreport.twig';
import './smmreport.yml';
import './sustainable-development-goals.twig';
import './sustainable-development-goals.yml';

export const name = 'demoPages';

export function disable() {}

export function enable(context = $(document), settings) {
  $('.homepage__header', context).css('color', settings.color);
  $('.homepage__text', context).html(
    'the header color should be overwritten by settings in pl/index.js. this text was overwritten from js in 06-pages/demo/index.js.'
  );
}

export default enable;
