/**
 * slider-default
 */
import $ from 'jquery';
import 'slick-carousel';

// Module dependencies

// Module styles
import 'slick-carousel/slick/slick.scss';
import './_slider.scss';

// Module template
import './_slider.twig';

export const name = 'slider';
export function disable() {}
export function enable() {
  // Taken from https://gist.github.com/gordonbrander/2230317.
  const uniqeId = () =>
    Math.random()
      .toString(36)
      .substr(2, 9);

  $('.slider--default').each((i, element) => {
    const $slider = $(element);
    const $sliderNav = $slider.next('.slider--default-nav');
    const idSuffix = uniqeId();
    const sliderId = `slider--default-${idSuffix}`;
    const sliderNavId = `slider--default-nav-${idSuffix}`;

    // Abort if this slider is already initialized.
    if ($slider.hasClass('slick-initialized')) {
      return;
    }

    // Assign unique ids to slider + nav.
    $slider.attr('id', sliderId);
    $sliderNav.attr('id', sliderNavId);

    // Clone slider images as preview images for slider nav if no dedicated
    // preview images are present.
    if ($sliderNav.find('img').length === 0) {
      $slider
        .find('img')
        .clone()
        .addClass('osce-figure-slide-thmb')
        .appendTo(`#${sliderNavId}`);
      $sliderNav.find('img.osce-figure-slide-thmb').wrap('<div />');
    }

    $slider.slick({
      slidesToShow: 1,
      focusOnSelect: true,
      autoplay: false,
      dots: false,
      nextArrow:
        '<button class="slider__next slider__arrow"><span>Next slide</span></button>',
      prevArrow:
        '<button class="slider__prev slider__arrow"><span>Previous slide</span></button>',
      asNavFor: `#${sliderId}, #${sliderNavId}`,
    });

    const slidesCount = $slider.slick('getSlick').slideCount;
    $sliderNav.slick({
      slidesToShow: slidesCount < 4 ? slidesCount : 4,
      centerMode: true,
      focusOnSelect: true,
      arrows: true,
      dots: false,
      infinite: true,
      responsive: [
        {
          breakpoint: 832,
          settings: {
            slidesToShow: slidesCount < 3 ? slidesCount : 3,
          },
        },
      ],
      asNavFor: `#${sliderId}, #${sliderNavId}`,
      nextArrow:
        '<button class="slider__next slider--default-nav__next slider--default-nav__arrow"><span>Next slide</span></button>',
      prevArrow:
        '<button class="slider__prev slider--default-nav__prev slider--default-nav__arrow"><span>Previous slide</span></button>',
    });
  });
}
export default enable;
