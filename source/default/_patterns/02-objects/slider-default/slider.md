---
el: '.slider'
title: 'Slider Default'
---

`source/_patterns/02-objects/slider-default/_slider.twig`

##Usage:
```
{% include '@objects/slider-default/_slider.twig' %}
```
or 
```
{% include '@objects/slider-default/_slider.twig'
with {
slider_type: 'default',
items: your_slider_items,
items_thumbs: your_slider_items_thumbs,
}
%}
```
