/**
 * Demo of slider. Pulls in slider assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './sliders.twig';
import yaml from './sliders.yml';

export default {
  twig,
  yaml,
};
