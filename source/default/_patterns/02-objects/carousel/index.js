/**
 * carousel
 */
import $ from 'jquery';
import 'slick-carousel';

// Module dependencies

import 'slick-carousel/slick/slick.scss';
import './_carousel.scss';
import './_carousel.twig';

export const name = 'carousel';

export function disable() {}

export function enable() {
  $('.carousel').each((i, element) => {
    const $slideshow = $(element)
      .find('.carousel__items')
      .first();

    // Add a class carousel__item to slides to allow proper BEM Syntax.
    $slideshow.on('init', () => {
      $slideshow.find('.slick-slide').addClass('carousel__item');
    });

    $slideshow.slick({
      infinite: true,
      slidesToScroll: 1,
      slidesToShow: 1,
      respondTo: 'slider',
      mobileFirst: true,
      responsive: [
        {
          breakpoint: 14 * 2 * 22,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
          },
        },
        {
          breakpoint: 14 * 22 * 3,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 3,
          },
        },
      ],
      dots: true,
      dotsClass: 'carousel__dots',
      appendArrows: $(element).find('.carousel__arrows'),
      appendDots: $(element).find('.carousel__controls'),
      nextArrow:
        '<button class="carousel__next carousel__arrow slick-prev">\n' +
        '  <i class="icon--triangle-right"></i>\n' +
        '</button>',
      prevArrow:
        '<button class="carousel__prev carousel__arrow">\n' +
        '  <i class="icon--triangle-left"></i>\n' +
        '</button>',
    });
  });
}

export default enable;
