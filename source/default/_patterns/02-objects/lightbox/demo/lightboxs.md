---
el: 'lightbox'
title: 'lightbox'
---

`source/_patterns/elements/lightbox/_lightbox.twig`

##Usage:

```
{% include '@elements/lightbox/_lightbox.twig' %}
```

or


```
{% include '@elements/lightbox/_lightbox.twig' 
with {
varname: '',
}
%}
```
