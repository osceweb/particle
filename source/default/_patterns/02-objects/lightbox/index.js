/**
 * lightbox
 */

import $ from 'jquery';

// Module dependencies
import 'protons';

// Module styles
import './_lightbox.scss';

// Module template
import './_lightbox.twig';

export const name = 'lightbox';

export const defaults = {};

/**
 * Components may need to run clean-up tasks if they are removed from DOM.
 *
 * @param {jQuery} $context - A piece of DOM
 * @param {Object} settings - Pertinent settings
 */
// eslint-disable-next-line no-unused-vars
export function disable($context, settings) {}

/**
 * Default colorbox params, used for standalone lightboxes and justified galleries.
 */
export const params = {
  maxWidth: '80%',
  maxHeight: '80%',
  opacity: 0.8,
  transition: 'elastic',
  current: '',
  className: 'lightbox',
  close:
    '<div class="lightbox__close lightbox__button"><span>Close</span></div>',
  next: '<div class="lightbox__next lightbox__button"><span>Next</span></div>',
  previous:
    '<div class="lightbox__prev lightbox__button"><span>Previous</span></div>',
  onComplete: cb => {
    let caption = $(cb.el)
      .find('.caption')
      .html();
    if (!caption) {
      caption = $(cb.el)
        .find('img')
        .attr('title');
    }
    $('#cboxLoadedContent').append(
      `<div class="lightbox__caption">${caption}</div>`
    );
  },
};

/**
 * Each component has a chance to run when its enable function is called. It is
 * given a piece of DOM ($context) and a settings object. We destructure our
 * component key off the settings object and provide an empty object fallback.
 * Incoming settings override default settings via Object.assign().
 *
 * @param {jQuery} $context - A piece of DOM
 * @param {Object} settings - Settings object
 */
// eslint-disable-next-line no-unused-vars
export function enable($context, { lightbox = {} }) {
  $('.open-lightbox').colorbox(params);
}

export default enable;
