---
el: 'audio'
title: 'audio'
---

`source/_patterns/objects/audio/_audio.twig`

##Usage:

```
{% include '@objects/audio/_audio.twig' %}
```

or


```
{% include '@objects/audio/_audio.twig' 
with {
embed: 'SoundCloud embed code',
}
%}
```
