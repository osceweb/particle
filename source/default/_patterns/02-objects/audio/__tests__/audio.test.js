import { name } from '..';

test('audio component is registered', () => {
  expect(name).toBe('audio');
});
