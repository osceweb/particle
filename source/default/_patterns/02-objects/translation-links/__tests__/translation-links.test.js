import { name } from '..';

test('translation-links component is registered', () => {
  expect(name).toBe('translationLinks');
});
