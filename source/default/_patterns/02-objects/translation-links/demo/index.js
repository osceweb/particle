/**
 * Demo of translationLinks. Pulls in translationLinks assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'objects/translation-links';

// Import demo assets
import twig from './translation-linkss.twig';
import yaml from './translation-linkss.yml';
import markdown from './translation-linkss.md';

export default {
  twig,
  yaml,
  markdown,
};
