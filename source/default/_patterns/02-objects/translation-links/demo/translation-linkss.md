---
el: 'translation-links'
title: 'Translation links'
---

`source/_patterns/objects/translation-links/_translation-links.twig`

##Usage:

```
{% include '@objects/translation-links/_translation-links.twig' %}
```

or


```
{% include '@objects/translation-links/_translation-links.twig' 
with {
links: {en: {title: 'English', uri: '/pc/116747', label: 'Permanent Council Decision No. 1117', active: 'active'}}
}
%}
```
