/**
 * mainNavigation
 */

// Module dependencies
import 'protons';

// Module styles
import './_main-navigation.scss';

// Module template
import './_main-navigation.twig';

export const name = 'mainNavigation';

export function disable() {}

export function enable() {}

export default enable;
