---
el: '.main-navigation'
title: 'Main navigation'
---

Specific styling for a `.menu` object placed in the `.page__navigation` area.

`source/_patterns/02-objects/main-navigation/_main-navigation.twig`

##Usage:

```
{% include '@objects/main-navigation/_main-navigation.twig' %}
```
