/**
 * Demo of mainNavigation. Pulls in mainNavigation assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './main-navigations.twig';
import yaml from './main-navigations.yml';

export default {
  twig,
  yaml,
};
