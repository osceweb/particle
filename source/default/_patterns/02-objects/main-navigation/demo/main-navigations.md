---
el: '.main-navigations'
title: 'Main navigation'
---

`source/_patterns/02-objects/main-navigation/_main-navigation.twig`

##Usage:

```
{% include '@objects/main-navigation/_main-navigation.twig' %}
```

or

```
{% include '@objects/main-navigation/_main-navigation.twig'
with {
items: your_items,
}
%}
```
