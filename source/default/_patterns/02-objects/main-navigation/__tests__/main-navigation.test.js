import { name } from '..';

test('main-navigation component is registered', () => {
  expect(name).toBe('main-navigation');
});
