---
el: '.social-media-follow-source'
title: 'Social Media: Follow the source'
---

`source/_patterns/02-objects/social-media/_social-media-follow-source.twig`

##Usage:

```
{% include '@objects/social-media/_social-media-follow-source.twig' %}
```
