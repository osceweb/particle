/**
 * Demo of socialMedia. Pulls in socialMedia assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './social-media-shares.twig';
import yaml from './social-media-shares.yml';

export default {
  twig,
  yaml,
};
