/**
 * socialMedia
 */

// Module dependencies
import 'protons';

// Module styles
import './_social-media.scss';

// Module template
import './_social-media-share.twig';
import './_social-media-follow-us.twig';
import './_social-media-follow-source.twig';
import './_social-media-follow-source-links.twig';
import './social-media.ui_patterns.yml';
import $ from 'jquery';

export const name = 'socialMedia';

export function disable() {}

export function enable() {
  // Taken from https://github.com/heiseonline/shariff/blob/develop/src/js/shariff.js.
  $('.social-media').on('click', '[data-rel="popup"]', e => {
    e.stopPropagation();
    e.preventDefault();
    const url = $(e.currentTarget).attr('href');
    const w = global.window;

    console.log('url', url);
    // If a twitter widget is embedded on current site twitter's widget.js
    // will open a popup so we should not open a second one.
    if (url.match(/twitter\.com\/intent\/(\w+)/)) {
      /* eslint-disable-next-line no-underscore-dangle */
      if (w.__twttr && w.__twttr.widgets && w.__twttr.widgets.loaded) {
        return;
      }
    }

    w.open(url, '_blank', 'width=600,height=460');
  });
}

export default enable;
