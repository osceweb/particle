/**
 * files
 */

// Module dependencies
import 'protons';

// Module styles
import './_files.scss';

// Module template
import './_files.twig';

export const name = 'files';

export function disable() {}

export function enable() {}

export default enable;
