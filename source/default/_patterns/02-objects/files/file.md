---
el: '.file'
title: 'File'
---

A download file list.

`source/_patterns/02-objects/files/_files.twig`

##Usage:

```
{% include '@objects/files/_files.twig' %}
```

or

```
{% include '@objects/file.twig'
with {
file_url: "#",
file_type: 'application/pdf',
file_title: 'OSCE-Leaks',
file_info: '(PDF, English, 1.79MB)',
file_state: 'is-closed',
}
%}
```
