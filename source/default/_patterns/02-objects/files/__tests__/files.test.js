import { name } from '..';

test('files component is registered', () => {
  expect(name).toBe('files');
});
