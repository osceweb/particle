---
el: '.files'
title: 'File'
---

A download file list.

`source/_patterns/02-objects/files/_files.twig`

##Usage:

```
{% include '@objects/files/_files.twig' %}
```
