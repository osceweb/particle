/**
 * Demo of files. Pulls in files assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './multiple_files.twig';
import yaml from './multiple_files.yml';

export default {
  twig,
  yaml,
};
