/**
 * bleedingLabel
 */

// Module dependencies

// Module styles
import './_bleeding-label.scss';

// Module template
import './_bleeding-label.twig';

export const name = 'bleedingLabel';

export function disable() {}

export function enable() {}

export default enable;
