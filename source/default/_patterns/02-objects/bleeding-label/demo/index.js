/**
 * Demo of bleedingLabel. Pulls in bleedingLabel assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './bleeding-labels.twig';
import yaml from './bleeding-labels.yml';

export default {
  twig,
  yaml,
};
