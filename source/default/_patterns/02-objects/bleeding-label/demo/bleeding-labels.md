---
el: '.bleeding-labels'
title: 'Bleeding label'
---

`source/_patterns/02-objects/bleeding-label/_bleeding-label.twig`

##Usage:

```
{% include '@objects/bleeding-label/_bleeding-label.twig'
    with {
    bleeding_label: 'This is a label',
}
%}
```

or

```
{% include '@objects/bleeding-label/_bleeding-label.twig'
with {
bleeding_label: 'This is a label',
bleeding_label_style: 'border: 1px solid black; width: 100%; height: 100px; position: relative',
bleeding_label_class: 'bleeding-label',
bleeding_label_content: 'Your content',
}
%}
```

or

```
{% embed '@objects/_bleeding-label.twig' %}
    {% block block_bleeding_label %}
      Whatever
    {% endblock block_bleeding_label %}
{% endembed %}
```

If you need some special styling, ...

Example for homepage (after the slider):


```
{% embed '@objects/_bleeding-label.twig'
with {
bleeding_label: 'Featured',
bleeding_label_style: 'clear: both;top: -60px;margin-bottom: -60px;margin-left: 40px;padding-left: 20px;position: relative;background: #fff;border-left: 1px solid #ccc;',
bleeding_label_class: 'bleeding-label',
}
%}
    {% block block_bleeding_label %}
    {% include "@objects/headlines/_headlines.twig"
      with {
      heading_level: '2',
      heading_content: '25th OSCE Ministerial Council',
    }
    %}
    <p>Ambition and realism to define Slovak OSCE Chairmanship in promoting dialogue, trust and stability in the OSCE area, while supporting the good functioning of the organization</p>
    {% endblock block_bleeding_label %}
{% endembed %}
```
