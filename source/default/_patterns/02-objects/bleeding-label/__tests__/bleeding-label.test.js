import { name } from '..';

test('bleeding-label component is registered', () => {
  expect(name).toBe('bleeding-label');
});
