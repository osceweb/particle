---
el: 'accordions'
title: 'accordion'
---

`source/_patterns/objects/accordion/_accordion.twig`

##Usage:

### Simple 
```
{% include '@objects/_accordion.twig'
  with {
  accordion_title: 'YOUR ACCORDION_TITLE (Header)',
  accordion_content: 'YOUR CONTENT',
  accordion_color: use 'dark' for dark mode, or leave empty for default colors. 
}
%}

```

or

### Nested


```
{% include '@objects/_accordion.twig'
with {
  accordion_nested: true,
  accordion_title: 'YOUR ACCORDION_TITLE (Header)'
}
%}

```
