---
el: 'widget'
title: 'widget'
---

`source/_patterns/objects/widget/_widget.twig`

##Usage:

```
{% include '@objects/widget/_widget.twig' %}
```

or


```
{% include '@objects/widget/_widget.twig' 
with {
varname: '',
}
%}
```
