/**
 * Demo of widget. Pulls in widget assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'objects/widget';

// Import demo assets
import twig from './widgets.twig';
import yaml from './widgets.yml';
import markdown from './widgets.md';

export default {
  twig,
  yaml,
  markdown,
};
