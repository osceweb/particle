/**
 * widget
 */
// Module dependencies
import 'protons';

// Module styles
import './_widget.scss';

// Module template
import './_widget.twig';

export const name = 'widget';

export function disable() {}

export function enable() {}

export default enable;
