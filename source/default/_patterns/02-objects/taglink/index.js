/**
 * taglink
 */

// Module dependencies
import 'protons';

// Module styles
import './_taglink.scss';

// Module template
import './_taglink.twig';

export const name = 'taglink';

export function disable() {}

export function enable() {}

export default enable;
