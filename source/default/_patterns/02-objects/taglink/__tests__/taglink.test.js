import { name } from '..';

test('taglink component is registered', () => {
  expect(name).toBe('taglink');
});
