---
el: '.taglinks'
title: 'Taglink'
---

`source/_patterns/02-objects/taglink/_taglink.twig`

##Usage:

```
{% include '@objects/taglink/_taglink.twig' %}
```
or
```
{% include '@objects/taglink/_taglink.twig'
with {
rows: yourtaglinks,
}
%}
```