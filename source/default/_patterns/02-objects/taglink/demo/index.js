/**
 * Demo of taglink. Pulls in taglink assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './taglinks.twig';
import yaml from './taglink.yml';

export default {
  twig,
  yaml,
};
