---
el: 'images'
title: 'Photo'
---

`source/_patterns/objects/image/_image.twig`

##Usage:

```
{% include '@objects/image/_image.twig' %}
```

or


```
{% include '@objects/image/_image.twig' 
with {
figure_image: 'Your image',
figure_caption: 'Your image caption text',
figure_image_detail_url: '',
}
%}
```
