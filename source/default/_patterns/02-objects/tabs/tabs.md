---
el: '.tabs'
title: 'Tabs'
---

`source/_patterns/02-objects/tabs/tabs.twig`

##Usage:

```
{% include '@objects/tabs/tabs.twig' %}
```

or

```
{% include '@objects/tabs/tabs.twig'
with {
  tabs: dummy_tabs,
}
%}
```
