import { name } from '..';

test('tabs component is registered', () => {
  expect(name).toBe('tabs');
});
