/**
 * tabs
 */

// Module dependencies
import 'protons';

// Module styles
import './_tabs.scss';

// Module template
import './tabs.twig';

export const name = 'tabs';

export function disable() {}

export function enable() {}

export default enable;
