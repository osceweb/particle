/**
 * messages
 */

// Module dependencies
import 'protons';

// Module styles
import './_message.scss';

// Module template
import './01-message.twig';

export const name = 'message';

export function disable() {}

export function enable() {}

export default enable;
