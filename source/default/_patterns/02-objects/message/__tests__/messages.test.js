import { name } from '..';

test('messages component is registered', () => {
  expect(name).toBe('messages');
});
