---
el: '.messages'
title: 'Messages'
---

`source/_patterns/02-objects/message/01-message.twig`

##Usage:

```
{% include '@objects/message/01-message.twig' %}
```

or

```
{% include '@objects/message/01-message.twig'
with {
message_type: 'status',
message_content: 'This is a status message',
}
%}
```

or

```
{% include '@objects/message/01-message.twig'
with {
message_type: 'warning',
message_content: 'This is a warning message',
}
%}
```

or

```
{% include '@objects/message/01-message.twig'
with {
message_type: 'error',
message_content: 'This is an error message',
}
%}
```
