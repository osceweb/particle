---
el: '.alerts'
title: 'Alerts'
---

`source/_patterns/02-objects/alert/_alert.twig`

##Usage:

```
{% include '@objects/alert/_alert.twig' %}
```

```
{% include '@objects/alert/_alert.twig' with alert_info %}
```

```
{% include '@objects/alert/_alert.twig' with alert_warning %}
```
