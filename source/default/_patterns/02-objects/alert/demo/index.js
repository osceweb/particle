/**
 * Demo of alert. Pulls in alert assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './alerts.twig';
import yaml from './alerts.yml';

export default {
  twig,
  yaml,
};
