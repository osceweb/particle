---
el: '.alert'
title: 'Alert'
---

`source/_patterns/02-objects/alert/_alert.twig`

##Usage:

```
{% include '@objects/_alert.twig' %}
```

```
{% include '@objects/_alert.twig' with alert_info %}
```

```
{% include '@objects/_alert.twig' with alert_warning %}
```
