/**
 * alert
 */

// Module dependencies

// Module styles
import './_alert.scss';

// Module template
import './_alert.twig';

export const name = 'alert';

export function disable() {}

export function enable() {}

export default enable;
