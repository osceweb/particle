/**
 * metaNavigation
 */

// Module dependencies
import 'protons';

// Module styles
import './_meta-navigation.scss';

// Module template
import './_meta-navigation.twig';

import $ from 'jquery';

export const name = 'metaNavigation';

export function disable() {}

export function enable() {
  $('.meta-nav__link-last').click(() => {
    $('.meta-nav__item-last').toggleClass('active-trail');
    $('.site--search').toggleClass('expanded');
  });
  $('#site--search-close').click(() => {
    $('.meta-nav__item-last').toggleClass('active-trail');
    $('.site--search').toggleClass('expanded');
  });
}

export default enable;
