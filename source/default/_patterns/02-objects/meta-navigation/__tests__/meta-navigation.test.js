import { name } from '..';

test('meta-navigation component is registered', () => {
  expect(name).toBe('meta-navigation');
});
