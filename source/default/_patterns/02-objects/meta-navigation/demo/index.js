/**
 * Demo of metaNavigation. Pulls in metaNavigation assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './meta-navigations.twig';
import yaml from './meta-navigations.yml';

export default {
  twig,
  yaml,
};
