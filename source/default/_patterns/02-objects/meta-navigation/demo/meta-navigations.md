---
el: '.meta-navigations'
title: 'Meta navigation'
---

Specific styling for a `.menu` object placed in the `.page__header__meta` area.

`source/_patterns/02-objects/meta-navigation/_meta-navigation.twig`

##Usage:

```
{% include '@objects/meta-navigation/_meta-navigation.twig' %}
```

or

```
{% include '@objects/meta-navigation/_meta-navigation.twig'
with {
items: meta_navigation_items,
}
%}
```
