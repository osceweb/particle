import { name } from '..';

test('styled-tabs component is registered', () => {
  expect(name).toBe('styledTabs');
});
