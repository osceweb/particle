/**
 * styledTabs
 */

// import $ from 'jquery';

// Module dependencies
import 'protons';

// Module styles
import './_styled-tabs.scss';

// Module template
import './_styled-tabs.twig';
import './_styled-tabs-content-item.twig';
import './_styled-tabs-nav-item.twig';

/*
export const name = 'styledTabs';

export const defaults = {
  dummyClass: 'js-styledTabs-exists',
};
*/

/**
 * Components may need to run clean-up tasks if they are removed from DOM.
 *
 * @param {jQuery} $context - A piece of DOM
 * @param {Object} settings - Pertinent settings
 */
export function disable($context, settings) {
}

/**
 * Each component has a chance to run when its enable function is called. It is
 * given a piece of DOM ($context) and a settings object. We destructure our
 * component key off the settings object and provide an empty object fallback.
 * Incoming settings override default settings via Object.assign().
 *
 * @param {jQuery} $context - A piece of DOM
 * @param {Object} settings - Settings object
 */
export function enable($context, {styledTabs = {} }) {
  // Find our component within the DOM
  // const $styledTabs = $('.tabs', $context);
  // Bail if component does not exist
  // if (!$styledTabs.length) {}
  // Merge defaults with incoming settings
  // const settings = Object.assign(defaults, styledTabs);
  // An example of what could be done with this component
  // $styledTabs.addClass(settings.dummyClass);
}

/**
 * Project: Tabs
 * Author: Marek Zeman
 * Twitter: MarekZeman91
 * Site: http://marekzeman.cz
 * URL: https://codepen.io/MarekZeman91/details/ByYGaO/
 * License: MIT
 * Version: 1.2
 * Revision: 13.2.2016 // updated options
 */
function Tabs(selector, options) {
  if (!(this instanceof Tabs)) return new Tabs(selector, options);

  selector = selector + '' === selector ? selector : 'a[href^="#tab"]';
  options = options && options.hasOwnProperty ? options : {};

  this.options = {};

  var item;
  for (item in this.defaults)
    this.options[item] = (
    options[item] === undefined ? this.defaults[item] :
      options[item]
  );

  var links = document.querySelectorAll(selector);
  for (var i = 0; links[i]; i++) {
    this[this.push(links[i]) - 1].addEventListener('click', this);
  }

  if (this[this.options.activeTab || 0]) {
    this[this.options.activeTab || 0].click();
  }
}

Tabs.prototype = [];
Tabs.prototype.defaults = {
  contentActive: 'active',
  linkActive: 'active',
  allowClose: false,
  activeTab: 0,
};
Tabs.prototype.options = {};
Tabs.prototype.latest = null;
Tabs.prototype.handleEvent = function (event, content) {
  event.preventDefault();

  if (this.latest) {
    this.latest.classList.remove(this.options.linkActive);
    if (content = document.querySelector(this.latest.hash)) {
      content.classList.remove(this.options.contentActive);
    }
  }

  if (this.latest === event.target && this.options.allowClose) {
    this.latest = null;
    return;
  }

  this.latest = event.target;
  this.latest.classList.add(this.options.linkActive);
  if (content = document.querySelector(this.latest.hash)) {
    content.classList.add(this.options.contentActive);
  }
};

/* Nested tabs */
new Tabs('.tabs__nav--item-link', {
  contentActive: 'alive',
  allowClose: false,
  activeTab: 0,
}).forEach(function (tabLink) {
  new Tabs(tabLink.hash + ' .tabs__nav--item-link-nested'),
    {
      activeTab: 1,
    };
});

export default enable;
