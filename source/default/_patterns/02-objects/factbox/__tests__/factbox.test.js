import { name } from '..';

test('factbox component is registered', () => {
  expect(name).toBe('factbox');
});
