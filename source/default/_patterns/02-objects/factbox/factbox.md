---
el: '.factboxs'
title: 'factboxs'
---

`source/_patterns/02-objects/factbox/_factbox.twig`

Available for mobile, desktop, mobile retina & desktop retina (resize viewport to test)

##Usage:

```
{% include '@objects/factbox/_factbox.twig' %}
```

or

```
{% include '@objects/factbox/_factbox.twig'
with {
"factbox_title": "Centre facts and figures",
}
%}
```
