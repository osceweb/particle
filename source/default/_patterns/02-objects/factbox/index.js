/**
 * factbox
 */

// Module dependencies
import 'protons';

// Module styles
import './_factbox.scss';

// Module template
import './_factbox.twig';

export const name = 'factbox';

export function disable() {}

export function enable() {}

export default enable;
