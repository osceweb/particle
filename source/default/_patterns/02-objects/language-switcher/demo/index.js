/**
 * Demo of languageSwitcher. Pulls in languageSwitcher assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './language-switchers.twig';
import yaml from './language-switchers.yml';

export default {
  twig,
  yaml,
};
