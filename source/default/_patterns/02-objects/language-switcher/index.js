/**
 * languageSwitcher
 */

// Module dependencies
import 'protons';

// Module styles
import './_language-switcher.scss';

// Module template
import './_language-switcher.twig';
import './language-switcher.ui_patterns.yml';

export const name = 'languageSwitcher';

export function disable() {}

export function enable() {}

export default enable;
