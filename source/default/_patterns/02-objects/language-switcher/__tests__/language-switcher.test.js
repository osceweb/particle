import { name } from '..';

test('language-switcher component is registered', () => {
  expect(name).toBe('language-switcher');
});
