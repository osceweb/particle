import { name } from '..';

test('video component is registered', () => {
  expect(name).toBe('video');
});
