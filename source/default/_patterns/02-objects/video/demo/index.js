/**
 * Demo of video. Pulls in video assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'objects/video';

// Import demo assets
import twig from './videos.twig';
import yaml from './videos.yml';
import markdown from './videos.md';

export default {
  twig,
  yaml,
  markdown,
};
