---
el: 'video'
title: 'video'
---

`source/_patterns/objects/video/_video.twig`

##Usage:

```
{% include '@objects/video/_video.twig' %}
```

or


```
{% include '@objects/video/_video.twig' 
with {
video_id: 'The Youtube video id',
}
%}
```
