/**
 * hamburger
 */

// Module dependencies
import 'protons';

// Module styles
import './_hamburger.scss';

// Module template
import './_hamburger.twig';

export const name = 'hamburger';

export function disable() {}

export function enable() {}

export default enable;
