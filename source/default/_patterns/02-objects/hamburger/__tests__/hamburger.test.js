import { name } from '..';

test('hamburger component is registered', () => {
  expect(name).toBe('hamburger');
});
