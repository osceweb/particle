---
el: '.hamburgers-icon'
title: 'Hamburger with icon'
---
`/source/default/_patterns/02-objects/hamburger/_hamburger.twig`

##Usage:

```
{% include '@objects/hamburger/_hamburger.twig'
with {
hamburger_icon: 'true',
}
%}
```