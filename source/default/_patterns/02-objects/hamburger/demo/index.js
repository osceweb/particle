/**
 * Demo of hamburger. Pulls in hamburger assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './hamburgers.twig';
import yaml from './hamburgers.yml';

export default {
  twig,
  yaml,
};
