---
el: '.hamburgers'
title: 'Hamburger'
---



`/source/default/_patterns/02-objects/hamburger/_hamburger.twig`

##Usage:

```
{% include '@objects/hamburger/_hamburger.twig' %}
```

with icon after:

```
{% include '@objects/hamburger/_hamburger.twig'
with {
hamburger_icon: 'true',
}
%}
```