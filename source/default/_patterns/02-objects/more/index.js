/**
 * more
 */

// Module dependencies
import 'protons';

// Module styles
import './_more.scss';

// Module template
import './_more.twig';
import './more.ui_patterns.yml';
import $ from 'jquery';

export const name = 'more';

export function disable() {}

export function enable() {
  $('.more--apply-to-text').each((index, element) => {
    const $button = $(element);
    const $container = $(element).parent();
    const $toggleItems = $container.find('p:first').siblings();
    if ($toggleItems.length > 0) {
      $toggleItems.hide();

      $button.insertAfter($container.find('p:first'));
      $button.css('display', 'inline-block');
      $button.on('click', evt => {
        console.log($toggleItems);
        $toggleItems.show();
        evt.preventDefault();
        $button.hide();
      });
    }
  });
}

export default enable;
