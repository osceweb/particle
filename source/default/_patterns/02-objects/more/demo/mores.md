---
el: '.more'
title: 'More'
---

A simple "read more" style link.

`source/_patterns/02-objects/more/more.twig`

##Usage:

```
{% include '@objects/more/_more.twig'
with {
  readmore_text: 'view all news',
  readmore_url: 'test',
}
%}
```
If you want the more link but without an url (at tag)
see: teaser/teaserlist
```
{% include '@objects/more/_more.twig'
with {
  readmore_text: 'view all news',
  hide_link: 'yes',
}
%}
```

Use show more button in a body field to show only the first paragraph
```
<p>First paragraph</p>
<p>Second paragraph (will be hidden as soon as JS kicks in)</p>
{% include '@objects/more/_more.twig' with {
  readmore_type: "small",
  readmore_apply_to_text: true
} %}
%}
```
