/**
 * Demo of button. Pulls in button assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import component assets
// import 'elements/button';

// Import demo assets
import twig from './mores.twig';
import yaml from './mores.yml';
// import markdown from './buttons.md';

export default {
  twig,
  yaml,
  // markdown,
};
