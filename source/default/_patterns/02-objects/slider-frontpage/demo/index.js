/**
 * Demo of slider-frontpage. Pulls in slider assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './slider-frontpages.twig';
import yaml from './slider-frontpages.yml';

export default {
  twig,
  yaml,
};
