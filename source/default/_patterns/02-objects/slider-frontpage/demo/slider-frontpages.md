---
el: '.slider-frontpages'
title: 'Slider Frontpage'
---

`source/_patterns/02-objects/_slider-frontpage.twig`

##Usage:

```
{% include '@objects/_slider-frontpage.twig' %}
```
or 
```
{% include '@objects/slider-frontpage/_slider-frontpage.twig'
with {
slider_type: 'frontpage',
items: your_front_slider_items,
items_thumbs: your_front_slider_items_thumbs,
}
%}
```
