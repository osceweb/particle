import { name } from '..';

test('slider-frontpage component is registered', () => {
  expect(name).toBe('slider-frontpage');
});
