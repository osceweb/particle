/**
 * slider-frontpage
 */
import $ from 'jquery';
import 'slick-carousel';

// Module dependencies

// Module styles
import 'slick-carousel/slick/slick.scss';
import './_slider-frontpage.scss';

// Module template
import './_slider-frontpage.twig';

export const name = 'slider-frontpage';
export function disable() {}
export function enable() {
  $('.slider--frontpage').slick({
    slidesToShow: 1,
    focusOnSelect: true,
    autoplay: true,
    nextArrow:
      '<button class="slider__next slider__arrow"><span>Next slide</span></button>',
    prevArrow:
      '<button class="slider__prev slider__arrow"><span>Previous slide</span></button>',
    asNavFor: '.slider--frontpage-nav, .slider-content, .slider--frontpage',
    responsive: [
      {
        breakpoint: 800,
        settings: {
          dots: true,
          dotsClass: 'slider__dots',
        },
      },
      {
        breakpoint: 460,
        settings: {
          arrows: false,
          dots: false,
        },
      },
    ],
  });

  $('.slider--frontpage-nav').slick({
    slidesToShow: 4,
    rows: 0,
    centerMode: true,
    centerPadding: '0px',
    focusOnSelect: true,
    vertical: true,
    verticalSwiping: true,
    arrows: false,
    asNavFor: '.slider--frontpage-nav, .slider-content, .slider--frontpage',
    nextArrow:
      '<button class="slider__next slider__arrow"><span>Next slide</span></button>',
    prevArrow:
      '<button class="slider__prev slider__arrow"><span>Previous slide</span></button>',
  });
  $('.slider--frontpage-content').slick({
    slidesToShow: 1,
    centerMode: true,
    focusOnSelect: true,
    arrows: false,
    rows: 0,
    asNavFor: '.slider--frontpage-nav, .slider-content, .slider--frontpage',
    accessibility: false,
    draggable: false,
    fade: true,
    cssEase: 'cubic-bezier(0.9, 0, 0.3, 1)',
  });
}
export default enable;
