import { name } from '..';

test('call-to-action component is registered', () => {
  expect(name).toBe('call-to-action');
});
