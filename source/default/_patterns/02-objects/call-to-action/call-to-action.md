---
el: '.call-to-action'
title: 'Call To Action'
---

A "call to action" style link.

`source/_patterns/02-objects/call-to-action/call-to-action.twig`

##Usage:

```
{% include '@objects/call-to-action/call-to-action.twig' %}
```

or

```
{% include '@objects/call-to-action/call-to-action.twig'
with {
call_to_action_class_name: 'call2action',
call_to_action_text: 'click me',
call_to_action_url: '#',
}
%}
```
