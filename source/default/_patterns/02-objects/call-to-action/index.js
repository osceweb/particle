/**
 * callToAction
 */

// Module dependencies

// Module styles
import './_call-to-action.scss';

// Module template
import './call-to-action.twig';

export const name = 'callToAction';

export function disable() {}

export function enable() {}

export default enable;
