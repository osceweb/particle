import { name } from '..';

test('author_date-info component is registered', () => {
  expect(name).toBe('author-date-info');
});
