---
el: 'author-date-infos'
title: 'author-date-info'
---

`source/_patterns/objects/author-date-info/_author-date-info.twig`

##Usage:

```
{% include '@objects/author-date-info/_author-date-info.twig' %}
```
or
```
{% include '@objects/author-date-info/_author-date-info.twig' 
with {
author: 'Author name',
author_date: 'Author date',
}
%}
```
