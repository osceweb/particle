/**
 * Demo of author-date-info. Pulls in author-date-info assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'objects/author-date-info';

// Import demo assets
import twig from './author-date-infos.twig';
import yaml from './author-date-infos.yml';
import markdown from './author-date-infos.md';

export default {
  twig,
  yaml,
  markdown,
};
