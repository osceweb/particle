---
el: '.download'
title: 'Download'
---

A download button

`source/_patterns/02-objects/download/download.twig`

##Usage:

```
{% include '@objects/download/download.twig' %}
```
