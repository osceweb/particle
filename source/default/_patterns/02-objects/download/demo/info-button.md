---
el: '.info-button'
title: 'Info Button'
---


`source/_patterns/02-objects/download/download.twig`

##Usage:

```
{% include '@objects/download/download.twig'
  with {
  file_name: 'OSCE-Document.ppt',
  svgname: 'mail',
  file_title: 'OSCE Document',
  url: '#',
  download_label: 'Contact: Request for bidding documents',
  target: '_blank',
  class: 'is-requestable',
  download_icon_before: 'true',
}
%}
```
