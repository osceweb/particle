/**
 * Demo of download. Pulls in download assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './downloads.twig';
import yaml from './downloads.yml';

export default {
  twig,
  yaml,
};
