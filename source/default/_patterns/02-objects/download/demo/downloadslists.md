---
el: '.downloads'
title: 'Downloads list'
---

A downloads list.

`source/_patterns/02-objects/download/_download_list.twig`

##Usage:

```
{% embed '@objects/download/_download_list.twig' %}
  {% block downloadslist %}
    {% include '@objects/download/download.twig'
      with {
      file_name: 'OSCE-Document name',
      svgname: 'download',
      file_title: 'OSCE Document title',
      url: '#',
      download_label: 'English',
      langcode: 'en',
      target: '_blank',
      size: '',
      language: '',
      mime: '',
      download_icon_after: 'true',
      icon_border: 'true',
    }
    %}
    {% include '@objects/download/download.twig'
      with {
      file_name: 'OSCE-Document name',
      svgname: 'download',
      file_title: 'OSCE Document title',
      url: '#',
      download_label: ' Русский',
      langcode: 'ru',
      target: '_blank',
      size: '',
      language: '',
      mime: '',
      download_icon_after: 'true',
      icon_border: 'true',
    }
    %}
    {% include '@objects/download/download.twig'
      with {
      file_name: 'OSCE-Document name',
      file_title: 'OSCE Document title',
      url: '#',
      download_label: 'Українська',
      langcode: 'uk',
      target: '_blank',
      size: '',
      language: '',
      mime: '',
      download_icon_after: 'true',
      icon_border: 'true',
    }
    %}
  {% endblock downloadslist %}
{% endembed %}

```
