---
el: '.downloads'
title: 'Downloads'
---

A download button.

`source/_patterns/02-objects/download/download.twig`

##Usage:

You can add icon before or after:

`download_icon_before: 'true',`

or

`download_icon_after: 'true',`


You can add a border to the icon:

`icon_border: 'true',`

```
{% 'include @objects/download/download.twig' %}
```
or
```
{% include '@objects/download/download.twig'
with {
file_name: 'OSCE-Document.pdf',
svgname: 'download',
file_title: 'OSCE Document',
url: 'http://example.com/OSCE-Document.pdf',
mime: 'application/pdf',
language: 'English',
download_label: 'Click here to download file',
langcode: 'en',
size: '314Kb',
target: '_blank',
}
%}
```
or 
```
{% include '@objects/download.twig'
with {
file_name: 'OSCE-Document.pdf',
svgname: 'download',
file_title: 'OSCE Document',
url: 'http://example.com/OSCE-Document.pdf',
mime: 'application/pdf',
language: 'English',
download_label: 'Click here to download file',
langcode: 'en',
size: '314Kb',
target: '_blank',
download_icon_before: 'true',
}
%}
```
