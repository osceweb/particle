import { name } from '..';

test('download component is registered', () => {
  expect(name).toBe('download');
});
