/**
 * download
 */

// Module dependencies

// Module styles
import './_download.scss';

// Module template
import './download.twig';
import './_download_list.twig';
import './download.ui_patterns.yml';

import './images/application-octet-stream.png';

export const name = 'download';

export function disable() {}

export function enable() {}

export default enable;
