---
el: '.logo'
title: 'Logo'
---

`source/_patterns/02-objects/logo/logo.twig`

Available for mobile, desktop, mobile retina & desktop retina (resize viewport to test)

##Usage:

```
{% include '@objects/logo/logo.twig' %}
```

```
{% 'include @objects/logo/logo.twig'
with {
  language_value:  '',
  logo_link_classes: '',
  logo_title: '',
  logo_url: '',
  logo_text: '',
  logo_link_target: '',
}
%}
```
