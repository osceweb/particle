import { name } from '..';

test('logo component is registered', () => {
  expect(name).toBe('logo');
});
