/**
 * logo
 */

// Module dependencies
import 'protons';

// Module styles
import './_logo.scss';

// Module template
import './logo.twig';

export const name = 'logo';

export function disable() {}

export function enable() {}

export default enable;
