/**
 * menu
 */

// Module dependencies
import 'protons';

// Module styles
import './_menu.scss';

// Module template
import './_menu.twig';

export const name = 'menu';

export function disable() {}

export function enable() {}

export default enable;
