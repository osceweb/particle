/**
 * Demo of menu. Pulls in menu assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './menus.twig';
import yaml from './menus.yml';

export default {
  twig,
  yaml,
};
