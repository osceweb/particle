---
el: '.menu'
title: 'Menu'
---

`source/_patterns/02-objects/menu/_menu.twig`

Available for mobile, desktop, mobile retina & desktop retina (resize viewport to test)

##Usage:

```
{% include '@objects/menu/_menu.twig' %}
```

or

```
{% include '@objects/menu/_menu.twig'
with {
    items: your_menu_items,
}
%}
```
