import { name } from '..';

test('headlines component is registered', () => {
  expect(name).toBe('headlines');
});
