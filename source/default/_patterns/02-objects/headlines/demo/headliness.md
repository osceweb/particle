---
el: 'headlines'
title: 'Headlines'
---

`source/_patterns/objects/headlines/_headlines.twig`

##Usage:

```
{% include '@objects/headlines/_headlines.twig' %}
```
or

```
{% include "@objects/headlines/_headlines.twig"
with {
heading_level: '1',
heading_content: 'your heading text',
}
%}
```
or

```
{% include "@objects/headlines/_headlines.twig"
with {
heading_level: '1',
heading_content: 'your heading text',
heading_url: 'your url',
heading_class: 'your classname',
}
%}
```
or with an image:

```
{% include "@objects/headlines/_headlines.twig"
with {
heading_level: '1',
heading_content: 'your heading text',
heading_url: 'your url',
headline_image: 'Your Image url ',
heading_class: 'your classname',
}
%}
```
