/**
 * Demo of headlines. Pulls in headlines assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'objects/headlines';

// Import demo assets
import twig from './headliness.twig';
import yaml from './headliness.yml';
import markdown from './headliness.md';

export default {
  twig,
  yaml,
  markdown,
};
