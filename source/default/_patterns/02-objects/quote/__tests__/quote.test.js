import { name } from '..';

test('quote component is registered', () => {
  expect(name).toBe('quote');
});
