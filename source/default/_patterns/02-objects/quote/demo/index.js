/**
 * Demo of quote. Pulls in quote assets, and provides demo-only assets.
 *
 * (This file is NOT imported by the design system, but is included as part of
 * a Pattern Lab app.)
 */

// Import component assets
import 'objects/quote';

// Import demo assets
import twig from './quotes.twig';
import yaml from './quotes.yml';
import markdown from './quotes.md';

export default {
  twig,
  yaml,
  markdown,
};
