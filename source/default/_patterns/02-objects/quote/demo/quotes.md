---
el: 'quote'
title: 'quote'
---

`source/_patterns/objects/quote/_quote.twig`

##Usage:

```
{% include '@objects/quote/_quote.twig' %}
```

or


```
{% include '@objects/quote/_quote.twig' 
with {
varname: '',
}
%}
```
