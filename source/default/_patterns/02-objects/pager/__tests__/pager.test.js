import { name } from '..';

test('pager component is registered', () => {
  expect(name).toBe('pager');
});
