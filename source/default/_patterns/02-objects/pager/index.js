/**
 * pager
 */

// Module dependencies
import 'protons';

// Module styles
import './_pager.scss';

// Module template
import './_pager.twig';

export const name = 'pager';

export function disable() {}

export function enable() {}

export default enable;
