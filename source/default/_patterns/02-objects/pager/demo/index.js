/**
 * Demo of pager. Pulls in pager assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './pagers.twig';
import yaml from './pagers.yml';

export default {
  twig,
  yaml,
};
