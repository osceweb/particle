import { name } from '..';

test('language component is registered', () => {
  expect(name).toBe('language');
});
