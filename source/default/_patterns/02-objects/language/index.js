/**
 * language
 */

// Module dependencies
import 'protons';

// Module styles
import './_language.scss';

// Module template
import './_language.twig';

// UI Pattern
import './language.ui_patterns.yml';

export const name = 'language';

export function disable() {}

export function enable() {}

export default enable;
