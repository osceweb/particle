/**
 * Demo of languages. Pulls in languages assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './languages.twig';
import yaml from './languages.yml';

export default {
  twig,
  yaml,
};
