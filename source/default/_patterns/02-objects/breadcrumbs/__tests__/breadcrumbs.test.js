import { name } from '..';

test('breadcrumbs component is registered', () => {
  expect(name).toBe('breadcrumbs');
});
