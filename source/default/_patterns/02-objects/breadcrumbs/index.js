/**
 * breadcrumbs
 */

// Module dependencies

// Module styles
import './_breadcrumbs.scss';

// Module template
import './breadcrumbs.twig';

export const name = 'breadcrumbs';

export function disable() {}

export function enable() {}

export default enable;
