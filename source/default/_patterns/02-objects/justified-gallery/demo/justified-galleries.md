---
el: '.justified-gallerys'
title: 'justified-gallerys'
---

`source/_patterns/02-objects/justified-gallery/_justified-gallery.twig`

##Usage:
See @containers/figure-justified for more details.
```
{% embed "@objects/justified-gallery/_justified-gallery.twig" %}
        {% block justified_gallery %}
            {% for i in range(0, 8 * gallery) %}
                {% set image = random(images) %}
                {% include '@containers/_figure-justified.twig'
                    with {
                    figure_url: paths.assets ~ "/" ~ image,
                    figure_fallback_url: "/media/" ~ count,
                    figure_rel: gallery,
                    figure_thumbnail: '<img src="' ~ paths.assets ~ "/" ~ image ~ '" />',
                    figure_caption: "I'm <strong>Element #" ~ count ~ "</strong> caption!",
                }
                %}
                {% set count = count + 1 %}
            {% endfor %}
        {% endblock %}
{% endembed %}
```
