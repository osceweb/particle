/**
 * Demo of justified-gallery. Pulls in justified-gallery assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

import './img/800x800g.png';
import './img/900x450a.png';
import './img/900x450s.png';
import './img/900x540r.png';

// Import demo assets
import twig from './justified-galleries.twig';
import yaml from './justified-galleries.yml';

export default {
  twig,
  yaml,
};
