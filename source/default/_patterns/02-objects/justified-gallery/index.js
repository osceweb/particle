/**
 * justified-gallery
 */
import $ from 'jquery';
import 'justifiedGallery';
import 'jquery-colorbox';
import 'objects/lightbox';

// Module dependencies

// Module styles
import 'justifiedGallery/dist/css/justifiedGallery.css';
import './_justified-gallery.scss';

// Module template
import './_justified-gallery.twig';

export const name = 'justified-gallery';

export function disable() {}

export function enable() {
  $('.justified-gallery')
    .justifiedGallery({
      margins: 10,
      border: 0,
      rowHeight: 150,
    })
    .on('jg.complete', element => {
      $(element.target)
        .find('a.figure-justified--link')
        .colorbox({
          maxWidth: '80%',
          maxHeight: '80%',
          opacity: 0.8,
          transition: 'elastic',
          current: '',
          className: 'lightbox',
          close:
            '<div class="lightbox__close lightbox__button"><span>Close</span></div>',
          next:
            '<div class="lightbox__next lightbox__button"><span>Next</span></div>',
          previous:
            '<div class="lightbox__prev lightbox__button"><span>Previous</span></div>',
          onComplete: cb => {
            const $element = $(cb.el).parent('.figure-justified');
            const caption = $element.find('.caption').html();
            const captionDetail = $element
              .find('.figure-justified--detail')
              .html();
            console.log(captionDetail);
            $('#cboxLoadedContent').append(
              `<div class="lightbox__caption"><p>${caption} ${captionDetail}</p></div>`
            );
          },
        });
    });
}

export default enable;
