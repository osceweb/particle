/**
 * map
 */

// Module dependencies
import 'protons';

// Module styles
import './_map.scss';

// Module template
import './_map.twig';

export const name = 'map';

export function disable() {}

export function enable() {}

export default enable;
