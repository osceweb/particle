/**
 * Demo of map. Pulls in map assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './maps.twig';
import yaml from './maps.yml';

export default {
  twig,
  yaml,
};
