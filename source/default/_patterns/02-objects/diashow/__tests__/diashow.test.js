import { name } from '..';

test('diashow component is registered', () => {
  expect(name).toBe('diashow');
});
