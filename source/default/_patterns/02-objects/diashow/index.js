/**
 * diashow
 */
import $ from 'jquery';
import 'slick-carousel';

// Module dependencies

// Module styles
import 'slick-carousel/slick/slick.scss';
import './_diashow.scss';

// Module template
import './_diashow.twig';

export const name = 'diashow';

export function disable() {}

export function enable() {
  $('.diashow').each((i, element) => {
    const $slideshow = $(element)
      .find('.diashow__items')
      .first();

    $slideshow.slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      dotsClass: 'diashow__dots',
      appendArrows: $(element).find('.diashow__controls'),
      nextArrow:
        '<button class="diashow__next diashow__arrow slick-prev">\n' +
        '<div class="svgicon svgicon__default">\n' +
        '  <svg viewBox="0 0 500 500" class="svg svg__triangle-right" width="16" height="16">\n' +
        '      <use xlink:href="../../../assets/spritemap.svg?cacheBuster=33381291#sprite-triangle-right"></use>\n' +
        '  </svg>\n' +
        '</div>\n' +
        '</button>',
      prevArrow:
        '<button class="diashow__prev diashow__arrow">\n' +
        '<div class="svgicon svgicon__default">\n' +
        '  <svg viewBox="0 0 500 500" class="svg svg__triangle-left" width="16" height="16">\n' +
        '      <use xlink:href="../../../assets/spritemap.svg?cacheBuster=33381291#sprite-triangle-left"></use>\n' +
        '  </svg>\n' +
        '</div>\n' +
        '</button>',
    });
  });
}

export default enable;
