/**
 * Demo of diashow. Pulls in diashow assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './diashows.twig';
import yaml from './diashows.yml';

export default {
  twig,
  yaml,
};
