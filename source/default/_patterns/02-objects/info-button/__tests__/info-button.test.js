import { name } from '..';

test('info-button component is registered', () => {
  expect(name).toBe('info-button');
});
