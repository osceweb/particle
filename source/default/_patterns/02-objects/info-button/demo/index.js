/**
 * Demo of infoButton. Pulls in infoButton assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './info-buttons.twig';
import yaml from './info-buttons.yml';

export default {
  twig,
  yaml,
};
