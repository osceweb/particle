/**
 * infoButton
 */

// Module dependencies
import 'protons';

// Module styles
import './_info-button.scss';

// Module template
import './_info-button.twig';

export const name = 'info-button';

export function disable() {}

export function enable() {}

export default enable;
