/**
 * Demo of languagesDropdown. Pulls in languagesDropdown assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './languages-dropdowns.twig';
import yaml from './languages-dropdowns.yml';

export default {
  twig,
  yaml,
};
