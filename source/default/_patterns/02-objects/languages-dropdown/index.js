/**
 * languagesDropdown
 */

// Module dependencies
import 'protons';

// Module styles
import './_languages-dropdown.scss';

// Module template
import './_languages-dropdown.twig';
import $ from 'jquery';

export const name = 'languagesDropdown';

export function disable() {}

export function enable() {
  $('.osce-language-switch-need-dropdown').click(event => {
    $(event.currentTarget).toggleClass('expanded');
    $('.site--multilingual--wrapper').toggleClass('expanded');
    $('.site--multilingual--wrapper').css('display', 'block');
    if ($('.site--multilingual--wrapper').hasClass('expanded')) {
      $('.site--multilingual--wrapper').css('display', 'block');
    } else {
      $('.site--multilingual--wrapper').css('display', 'none');
    }
  });

  $('.multilingual-close').click(() => {
    if ($('.site--multilingual--wrapper').hasClass('expanded')) {
      $('.site--multilingual--wrapper').css('display', 'none');
      $('.site--multilingual--wrapper').removeClass('expanded');
      $('.osce-language-switch').removeClass('expanded');
    }
  });
}

export default enable;
