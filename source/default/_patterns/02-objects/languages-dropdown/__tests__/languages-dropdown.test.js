import { name } from '..';

test('languages-dropdown component is registered', () => {
  expect(name).toBe('languages-dropdown');
});
