/**
 * Demo of externalLink. Pulls in externalLink assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './external-links.twig';
import yaml from './external-links.yml';

export default {
  twig,
  yaml,
};
