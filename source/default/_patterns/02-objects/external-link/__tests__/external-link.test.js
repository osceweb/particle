import { name } from '..';

test('external-link component is registered', () => {
  expect(name).toBe('external-link');
});
