---
el: '.external-link'
title: 'External link'
---

An external link.

`source/_patterns/02-objects/external-link/external-link.twig`

##Usage:

```
{% include '@objects/external-link/external-link.twig' %}
```

or:

```
{% include '@objects/external-link/external-link.twig'
  with {
  external_link_title: "Link text",
  external_link_url_value: "your url",
}
%}
```
