/**
 * externalLink
 */

// Module dependencies
import 'protons';

// Module styles
import './_external-link.scss';

// Module template
import './_external-link.twig';

export const name = 'externalLink';

export function disable() {}

export function enable() {}

export default enable;
