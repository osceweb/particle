/**
 * factsheet
 */

// Module dependencies
import 'protons';

// Module styles
import './_factsheet.scss';

// Module template
import './_factsheet.twig';

export const name = 'factsheet';

export function disable() {}

export function enable() {}

export default enable;
