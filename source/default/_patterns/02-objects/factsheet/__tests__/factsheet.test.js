import { name } from '..';

test('factsheet component is registered', () => {
  expect(name).toBe('factsheet');
});
