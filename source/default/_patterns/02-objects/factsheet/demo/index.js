/**
 * Demo of factsheet. Pulls in factsheet assets, and provides demo-only assets.
 *
 * This file is NOT imported by design-system.js, but is included as part of apps/pl/index.js
 */

// Import demo assets
import twig from './factsheets.twig';
import yaml from './factsheets.yml';

export default {
  twig,
  yaml,
};
