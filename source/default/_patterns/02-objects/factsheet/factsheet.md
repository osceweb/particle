---
el: '.factsheets'
title: 'Factsheets'
---

`source/_patterns/02-objects/factsheet/_factsheet.twig`

Available for mobile, desktop, mobile retina & desktop retina (resize viewport to test)

##Usage:

```
{% include '@objects/factsheet/_factsheet.twig' %}
```

or

```
{% include '@objects/factsheet/_factsheet.twig'
with {
"factsheet_title": "Centre facts and figures",
}
%}
```
