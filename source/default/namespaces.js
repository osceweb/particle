/**
 * Share atomic concepts with Webpack, Gulp, Pattern Lab, Drupal, etc
 */

const path = require('path');

const namespaceMaker = require('../../tools/namespace-maker');

const sets = {
  protons: path.resolve(__dirname, '_patterns', '00-protons'),
  elements: path.resolve(__dirname, '_patterns', '01-elements'),
  objects: path.resolve(__dirname, '_patterns', '02-objects'),
  containers: path.resolve(__dirname, '_patterns', '03-containers'),
  composites: path.resolve(__dirname, '_patterns', '04-composites'),
  layouts: path.resolve(__dirname, '_patterns', '05-layouts'),
  pages: path.resolve(__dirname, '_patterns', '06-pages'),
};

const namespaces = namespaceMaker(path.resolve(__dirname, '_patterns'), sets);

module.exports = {
  sets,
  namespaces,
};
